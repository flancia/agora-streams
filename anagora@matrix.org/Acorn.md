- [[2022-12-17 13:02:47]] [[@doubleloop:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$CH-y-LzUBefev2-pWlIgTkPON7fY2nNiCXU8kOkpaX0)):
  - [[Acorn]] looks interesting - https://acorn.software/

P2P project management software built on [[Holochain]]
- [[2022-12-18 14:02:40]] [[@doubleloop:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$KwZD4WraTqJmnKVN_yec4zSJCuWG3b3rhDCGG7TEr00)):
  - flancian 🍄: would you be interested in trying out [[Acorn]] as a way of managing the development of the Agora?  (not the source code, but issue tracking, roadmap etc)
