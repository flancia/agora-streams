- [[2022-05-11 21:40:50]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$sIK4C0PnIViMAQJ49QIauLy0X5jwqoetA6wLKFjjup4)):
  - [[moa schema]]:
  - table [[bridge]] contains one row per moa user, with separate fields prefixed twittter_, mastodon_, instagram_ for auth tokens and last cross posted resource
  - [[bridgemetadata]] maps bridges to workers, keeps track of time of last tweet/toot
  - [[bridgestat]] has counts and such -- unsure why it's a separate table from the above
  - [[mapping]] contains the association of a mastodon *post* id and a twitter id, unsure why it doesn't have instagram (perhaps it was created after instagram broke?) 
  - [[mastodon host]] contains information about the instances
  - [[settings]] has all the checkboxes from the UI
  - [[workerstat]] does what it says in the tin :)

this is all managed with [[alembic]], which I still haven't learnt :) but mean to.
