- [[2023-03-06 08:50:04]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$R-Dd-q3gxANjZmA6ldnhs0S0G1rSONZvxJ0RsG0K1kw)):
  - That's true. What I meant, is that Markdown has both []() and [[]], while Mycomarkup has only [[]].

I call links like https://google.com auto links, because they are found automatically, without any syntax, besides the URL syntax.
