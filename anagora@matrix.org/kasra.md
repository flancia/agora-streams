- [[2022-05-01 00:37:41]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$jIwdPjwKOtauCH5bwLjbxhuaGfPezVR4Y9s5-SUc_Sw)):
  - thank you for spotting the connection, [[kasra]]! I agree that the Agora might yield something shaped like this; I commented in the thread and referred to you. do you have Twitter?
- [[2022-05-01 21:16:27]] [[@huggingpuppy:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$J4D-uNZTer_xYF2oul7-qe741cv2nGyJQVwcEK5WSws)):
  - just had a call with me ([[kasra]]) [[flancian]] and [[j0ecool]]

[[agora]] and the [[shamanic agora]] are insanely auspicious

a cheers to the open ethos 🥂

as it was, as it is, as it shall be ✨
- [[2022-05-02 01:45:40]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$uh9uMPPITiD7GY3YWl_Zm87DL2CQlNomDa4K8t6oTvk)):
  - I met with [[kasra]] and [[joe cool]] from [[shamanic coders guild]] today, took notes
- [[2022-05-21 17:44:55]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$BYvuvYTPdBHzUyAfmE6YwWpKXOKIPAGXVeu5q8OfURk)):
  - with [[kasra]] and [[real ez cheese]]
- [[2022-05-21 22:02:14]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$wFEdnSsw3_oN88Aag-FdDMG3hW0_2w6gLLxraZC-bbI)):
  - [[kasra]] and [[joecool]] are interested
