- [[2022-06-26 13:02:19]] [[@vera:fairydust.space]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$STb0Pkfv1CyDl8PHoaYkjF6uMXugzMDGZQ4xnm9WIRY)):
  - Looks like [[bonfire]] has [[nomadic identity]]

>Nomadic identity: You can switch/add apps, change device or host for your instance, while keeping the same user/domain name. You’ll also be able to migrate your identity to a different domain.
