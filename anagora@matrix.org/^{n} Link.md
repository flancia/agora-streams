- [[2023-05-29 04:19:56]] [[@sneakers-the-rat:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$168532679659427mcWrh:matrix.org)):
  - btw @[flancian 🍄] I have written a little bot to parse WikiLinks and embed them in mediawiki pages if we want to have an agorabot for social.coop wiki where we can do matrix -> wiki and fedi -> wiki 👀👀. I have found the mediawiki API actually p confusing but have gotten a simple create/insert on pages and sections using a mediawiki template. 

parser also has ability to do n-back WikiLinks to indicate that the bot should grab some range of posts that precede the wikilinked post like [[^{m,n} Link]] or [[^{n} Link]] (full syntax described in parser docstrings linked below. I've found that to be a common pattern, wanting to tag some conversation after the fact. Next need to make a command syntax to implement basic stuff like declaring checklists and having the bot retrieve them and whatnot to make the chat<->wiki bridge more bidirectional.

post to mediawiki: https://git.jon-e.net/jonny/wiki-postbot/src/branch/main/wiki_postbot/interfaces/mediawiki.py
WikiLink parser: https://git.jon-e.net/jonny/wiki-postbot/src/branch/main/wiki_postbot/patterns/wikilink.py

- [[2023-05-31 03:35:10]] [[@sneakers-the-rat:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$16854969107624cdikp:matrix.org)):
  - btw @[flancian 🍄] I have written a little bot to parse WikiLinks and embed them in mediawiki pages if we want to have an agorabot for social.coop wiki where we can do matrix -> wiki and fedi -> wiki 👀👀. I have found the mediawiki API actually p confusing but have gotten a simple create/insert on pages and sections using a mediawiki template. 

parser also has ability to do n-back WikiLinks to indicate that the bot should grab some range of posts that precede the wikilinked post like [[^{m,n} Link]] or [[^{n} Link]] (full syntax described in parser docstrings linked below. I've found that to be a common pattern, wanting to tag some conversation after the fact. Next need to make a command syntax to implement basic stuff like declaring checklists and having the bot retrieve them and whatnot to make the chat<->wiki bridge more bidirectional.

post to mediawiki: https://git.jon-e.net/jonny/wiki-postbot/src/branch/main/wiki_postbot/interfaces/mediawiki.py
WikiLink parser: https://git.jon-e.net/jonny/wiki-postbot/src/branch/main/wiki_postbot/patterns/wikilink.py

