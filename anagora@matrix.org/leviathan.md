- [[2022-08-10 19:28:04]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$J2G0cbEB4TA_owVca6vvpg2ttlwBy3WgYk2j69wbIpc)):
  - https://melanocarpa.lesarbr.es/hypha/leviathan [[leviathan]]

> Leviathan (לויתן) is one of the rulers of Babylon. It is a sea monster, a large serpent with impenetrable scales. Leviathan does not give you choice, it just does what it intends to. It can destroy your home, kill somebody, but it is not to blame, for it is more powerful, than you.

Do we have Leviathan in our mythology? A friend of [[Moloch]]'s, so it seems.
