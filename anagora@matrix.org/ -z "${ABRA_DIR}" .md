- [[2023-10-11 18:52:46]] [[@rix:cooperative.computer]] ([link](https://develop.element.io/#/room/!DfXPgKLoYCvjHithgS:autonomic.zone/$jpKauT0oSD9PZ3zJGB9sVy_xqNhJu_J_4mh067gPJ2U)):
  - If anyone gets a moment can they suggest something to fix the following error running integration tests:

bats -Tp tests/integration/app_new.bats
 ✗ setup_suite []
   (from function `teardown_suite' in test file tests/integration/setup_suite.bash, line 39)
     `if [[ -z "${ABRA_DIR}" ]]; then' failed with status 0
   bats warning: Executed 1 instead of expected 8 tests

The output of env | grep ABRA is as follows
ABRA_TEST_DOMAIN=test.example.com
ABRA_DIR=/home/rix/.abra_test

So variables seem to be ok. I tried running `apt install bats-file bats-assert bats-support jq make git` and got 

E: Unable to locate package bats-file
E: Unable to locate package bats-assert
E: Unable to locate package bats-support

On ubuntu so that could be the issue but I'm not sure.
