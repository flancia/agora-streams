- [[2022-11-20 22:55:55]] [[@bjeanes:beeper.com]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166898135528LabMu:beeper.com)):
  - Tangentially: words are hard. Personally, I long ago adopted the nomenclature from [[Simple made Easy]] (https://www.youtube.com/watch?v=SxdOUGdseq4) and believe that "complicated" is a completely measurable and objective quality.

Easy vs hard... definitely subjective.

When I speak about something being complicated, I specifically mean that it becomes impossible to think about X without also considering Y; the variables confound and complicate each other. 

Having the database and web app on the same server does this: we can't horizontally scale the web app, because we can't horizontally scale a Postgres database; we can't update the OS and run the app on it side-by-side to ensure stability before cutting over; etc
