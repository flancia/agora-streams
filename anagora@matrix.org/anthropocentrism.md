- [[2022-09-18 15:20:54]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$QFb9ssMzcPSa9Omff65QU6DeDL4kV9FaP3j805UmVWo)):
  - Compiled the discussion about [[anthropocentrism]] with protopian here:

https://melanocarpa.lesarbr.es/hypha/anthropocentrism
- [[2022-09-18 20:00:37]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$dQM-95gOa0dUnJYgcoR9mrPoK-GYTHmz_3OLcD0yxOw)):
  - I will make it so that [[anthropocentrism]] links to melanocarpa for your subnodes
