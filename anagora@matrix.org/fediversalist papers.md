- [[2025-02-20 17:31:07]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$174006906711638kJXnB:matrix.org)):
  - Thanks for raising! From what I read in the [[fediversalist papers]] CCS can be an expensive service to run, and what they posted confirms this:

> Suspend CCS: CCS and its CSAM detection and reporting service online is the most expensive project we operate, and will likely close between March 15 and March 30. The core technology requirements to simply operate the service exceed $60,000 per year, and that doesn’t include the legal advisory and content review support we need to bring this service to the Fediverse in a broader fashion.
