- [[2022-11-17 04:49:39]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$RzcEcviMRfbYpJE7kmWV1XrJvEHnVXfpgPSUYPCdVkI)):
  - I have a new project idea, a tool to manage your link collection, called [[Betula]], that can federate with other Betulae and Mycorrhizae, thus emerging the [[Mycoverse]].

I try not to jump into implementing it. I have a lot of assignments, including programming ones. If I work on Betula instead, I'll feel bad.

Accepted two patches to [[Mycorrhiza]] with [[SourceHut]] recently. Pretty fun.
- [[2023-09-23 16:20:57]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$62jV0yodGLB9OIQ0WWeqhxwA7syxTQV9UYDbDdzEF7c)):
  - [[Mycoverse]] was born today

https://fosstodon.org/@betula/111114850992671579
