- [[2022-06-26 12:58:45]] [[@vera:fairydust.space]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$_q_gDV-JudNJO0fgDOVu281ydxWFQAUDL-We-8XmaUQ)):
  - [[bonfire]] look really interesting

>We endeavour to develop tools for conviviality. Tools that you have total control over, which are not developed for extracting value from their users in any way. Each Bonfire instance is shaped by its members to pursue their community goals, not those of some investment fund.
- [[2022-06-26 13:02:19]] [[@vera:fairydust.space]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$STb0Pkfv1CyDl8PHoaYkjF6uMXugzMDGZQ4xnm9WIRY)):
  - Looks like [[bonfire]] has [[nomadic identity]]

>Nomadic identity: You can switch/add apps, change device or host for your instance, while keeping the same user/domain name. You’ll also be able to migrate your identity to a different domain.
- [[2022-06-26 16:21:49]] [[@_discord_708787219992805407:t2bot.io]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$GT7QA07Wk2eairFylbLhlqj3YQeaRevUuz373xe0-CA)):
  - Excited about checking out [[bonfire]] and [[you.com]] (that second is search and not social, they're just the two things at the top of my stack I think)
- [[2022-11-04 21:02:55]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$O8onXjijyh9MIzO1b1NUH8GFE71-KP8IriR0bXquDkk)):
  - moonlion.eth (vera) 🍄 was enthusiastic about [[bonfire]]
- [[2022-11-28 23:31:01]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$1669674661158061RbjBa:matrix.org)):
  - OK, so the plan is to install [[bonfire]] on a.social.coop (I like how it reads even ;))
- [[2023-02-17 12:54:04]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$1676634844277479IgiqO:matrix.org)):
  - Maybe [[bonfire]] will do this better (this seems likely)
- [[2023-03-11 18:59:44]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$1678557584130275BMCwc:matrix.org)):
  - then I intend to set up [[mediawiki]] and maybe [[bonfire]], but let's see how far I get ;)
- [[2023-03-24 12:04:07]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$8cwRu9WuJ8ObMQr7HTaFNnkyHcK3uPfdi0jLBAIIsXk)):
  - Fair although I really liked what I saw of elixir, and [[bonfire]] is written in it
- [[2023-04-03 02:27:22]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$168048164269546oSlew:matrix.org)):
  - WDYT about [[bonfire]]? bonfire.social.coop
