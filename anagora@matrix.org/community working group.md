- [[2022-10-09 12:21:41]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166531090194319mSdHA:matrix.org)):
  - I've discussed the extra/secondary/experimental server with the [[cwg]] ~ [[community working group]] last week
- [[2022-10-27 19:17:14]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$166689103484378LiIPY:matrix.org)):
  - fyi I'm the [[community working group]] OnCall this week, please let me know if I can help with anything all!
- [[2022-10-27 23:46:33]] [[@kalebpace:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$166690719392183EEUkn:matrix.org)):
  - > <@flancian:matrix.org> fyi I'm the [[community working group]] OnCall this week, please let me know if I can help with anything all!

I did have a question about the account migration feature: When I tried to migrate from mastodon.social to social.coop, i received errors about the account alias not being created on the old account, even though it was.
Is this a known issue or an intentional disabling on the social.coop instance? 
Not a huge deal, I had few things to move over. Mainly just curious how to find out. Thanks for your time by the way! 🙂
- [[2022-10-28 00:09:55]] [[@kalebpace:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$166690859592690zbXhR:matrix.org)):
  - > <@flancian:matrix.org> fyi I'm the [[community working group]] OnCall this week, please let me know if I can help with anything all!

I did have a question about the account migration feature: When I tried to migrate from mastodon.social to social.coop, i received errors about the account alias not being created on the old account, even though it was.

Is this a known issue or an intentional disabling on the social.coop instance? 

Not a huge deal, I had few things to move over. Mainly just curious how to find out. Thanks for your time by the way! 🙂
- [[2022-11-04 00:42:25]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$klESfH8mQg_mzcFvJ1kWREw-wtRF3DxzlKcz-WfOaj8)):
  - borismann: wdyt about bringing moa to the \[\[tech working group\]\] and [[community working group]] of social.coop?
