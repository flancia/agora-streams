- [[2022-04-15 11:59:19]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$ohnaP-2UQw8oTi-ADl0eJQlnYvcr-d30l9dSz2nhlpc)):
  - TIL about [[transcopyright]] by Ted Nelson, thanks charismatic_shell 🍄 
- [[2023-03-11 09:47:02]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$b_UBakt4BimJgE-0wlq2a8QZhSBm1CCXQHn4PAs9hlY)):
  - Added the [[transcopyright]] license to make the inclusion in Agora possible
