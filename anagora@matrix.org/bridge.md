- [[2022-03-20 13:10:13]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$nRLINNbyK8injATWJDDoLO0nu6_F5DL0giqcsIzCm_U)):
  - Something I noticed about [[bridge]]s is that different messaging software has different ideas. For example, [[IRC]] has single-line messages. When bridging with [[Telegram]], multi-line messages are sent as several messages. Also, images. Telegram has captions, [[Matrix]] does not. &c. Problems arise. But connecting different places sure is great
- [[2022-05-11 21:40:50]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$sIK4C0PnIViMAQJ49QIauLy0X5jwqoetA6wLKFjjup4)):
  - [[moa schema]]:
  - table [[bridge]] contains one row per moa user, with separate fields prefixed twittter_, mastodon_, instagram_ for auth tokens and last cross posted resource
  - [[bridgemetadata]] maps bridges to workers, keeps track of time of last tweet/toot
  - [[bridgestat]] has counts and such -- unsure why it's a separate table from the above
  - [[mapping]] contains the association of a mastodon *post* id and a twitter id, unsure why it doesn't have instagram (perhaps it was created after instagram broke?) 
  - [[mastodon host]] contains information about the instances
  - [[settings]] has all the checkboxes from the UI
  - [[workerstat]] does what it says in the tin :)

this is all managed with [[alembic]], which I still haven't learnt :) but mean to.
