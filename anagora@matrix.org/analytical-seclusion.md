- [[2022-03-12 22:41:41]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$-OVJA9Yp8w0HlTglMHuHkZ5OU9nNZZQsY93B33q_5YA)):
  - [[analytical seclusion]]

> For a couple of years, I go into a voluntary seclusion, during which I dedicate most of my time to analysis. I will mostly avoid creating new stuff, instead, I will polish the old stuff. Update, document, publish, research.

melanocarpa.lesarbr.es/hypha/a…
- [[2022-03-12 22:42:10]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$Vt8ClasUh60J777wIgjNfmIrT_lXtAr5fa4Qc0dLT5c)):
  - [[analytical seclusion]]

> For a couple of years, I go into a voluntary seclusion, during which I dedicate most of my time to analysis. I will mostly avoid creating new stuff, instead, I will polish the old stuff. Update, document, publish, research.

https://melanocarpa.lesarbr.es/hypha/analytical_seclusion
- [[2022-03-19 20:28:41]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$d1_RLDye8PFZHpvHbUr_2HKWMAYpNRtq1xzlszvY5rA)):
  - [[flancian]]: moved our discussion about [[analytical seclusion]] to the garden. If you are not ok with that, let me know

=> https://melanocarpa.lesarbr.es/hypha/analytical_seclusion
