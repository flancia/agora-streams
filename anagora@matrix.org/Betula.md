- [[2022-11-17 04:49:39]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$RzcEcviMRfbYpJE7kmWV1XrJvEHnVXfpgPSUYPCdVkI)):
  - I have a new project idea, a tool to manage your link collection, called [[Betula]], that can federate with other Betulae and Mycorrhizae, thus emerging the [[Mycoverse]].

I try not to jump into implementing it. I have a lot of assignments, including programming ones. If I work on Betula instead, I'll feel bad.

Accepted two patches to [[Mycorrhiza]] with [[SourceHut]] recently. Pretty fun.
- [[2022-12-10 21:36:21]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$sE5fUjbCu9vIc1rrDGhvin7y4MMo38ka5WFzXBohpOM)):
  - Finishing the semester is a hard work, but I'm managing to handle it. I will travel to Ufa, my hometown, to celebrate the New Year for a week, during which I hope to work in public on [[Betula]] in a similar fashion while I procrastinate on the exams.
- [[2023-02-19 19:52:13]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$kVbL0Z8Is9_1C7voyb_JLSNN2LQpQ7r3fUxLKzdCJHU)):
  - https://melanocarpa.lesarbr.es/hypha/betula#Simplest_bookmarklet

Made a simple bookmarklet for [[Betula]].

CC flancian 🍄 
- [[2023-03-08 14:25:40]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$vssOq2ArpMYTOnLzLflXhIs-GxSz2F0j5aK_xPocyfs)):
  - I've just understood that [[Betula]]'s website can be added to Agora of Flancia. Here's the git repo: https://git.sr.ht/~bouncepaw/betula-site

flancian 🍄: 
