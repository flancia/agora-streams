- [[2022-05-09 09:28:12]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$oNFWgzqcDMAbqdZQn6-NKtXdkcQCT0-EyBXwS5pkUf0)):
  - done with Twitter, plan to fix a bug tonight with hashtags+punctuation; and then work on the [[agora server]] side of things
- [[2022-06-03 22:18:34]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$nwAfso3rJ2lOuw3RRoUUBZjj4cZjJFXEvazMjw-dRiM)):
  - and [[agora server]] is basically a quickly coded [[node viewer]]
- [[2022-07-09 18:12:00]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$Pcp06kXvC000LqtG2oh4FgLybNWsd8ZHi9TB0Twoau4)):
  - I think it also helps keep [[agora server]] easy to run and scale
- [[2022-07-09 18:19:20]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$q0uWd8anKrF-bDC65pq5K4uahZTKRKyck7CuTjS6Tb4)):
  - it was to make sure that we kept [[agora server]] *free of write paths*
- [[2023-07-28 19:08:05]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$WyybEH9xwra5attC1EGNQf36-Q9afpEgn7aYvrhoyMU)):
  - ahoy! [[agora server]] and [[agora bridge]] are both mostly python (with some typescript thrown in), but the idea behind the Agora design is that integrations should be doable relatively independently of these codebases.
- [[2023-09-07 21:24:45]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$bK_oCCdyaqtEJCOrA_Jiafs4HUkYZOSzalKyXx7WSf0)):
  - but in [[agora server]] I've tried to keep a 'default handler' approach, where each URL brings up 'at least a node'
- [[2023-12-16 19:44:02]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!rEEWZlconAjVwxlZzy:matrix.org/$0fo78GR6OGrSdl6GnWgXFQ_-mVcAUSvfqMolNaw68DY)):
  - I've been meaning to set up a sort of [[prompt router]] within [[agora server]] for a while
- [[2024-08-12 17:30:48]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$avyXel-xjCaRAtDt-ncwqS3fchc_IbKOlU0zaY53mAg)):
  - [[agora server]] and [[agora bridge]] are 95% Python 😄
