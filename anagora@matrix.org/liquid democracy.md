- [[2022-07-01 23:26:23]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$yL3yCrQOF3YRr8qpOR4SxtM6ZZFrnq8_kHo8FqUGrWM)):
  - the trivial thing is to keep global counts, but I'd rather do it right from the start maybe? and implement [[liquid democracy]] ;)
- [[2022-09-17 14:36:25]] [[@wawalker:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$aZR1ZZN5sthrBlSXpdjSaJhjGOYfMNhQGHwVzWGMPFA)):
  - maybe the whole idea of “claiming” a planet seems silly, or self-centered, selfish, and unnecessary. why try to own the commons? 

similar to indigenous folks in north america who didn’t have the same views of property as the europeans. just because they didn’t have the same interest in claiming land, didn’t mean they were inherently less worthy or in any way lesser. 

i definitely think the anthropomorphic view is much more common generally. i think the anti is more selection bias. see: meat eating, animal welfare, overall actions towards the environment, etc. 

monarchist over here. didn’t you hear the queen passed? (long live the king?) power to the people (and other sentient beings? all about the democracy, especially [[liquid democracy]]! but as usual the question is who is part of “the people”. we’ve expanded from just land owning white men
