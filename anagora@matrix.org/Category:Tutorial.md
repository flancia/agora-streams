- [[2023-07-06 02:53:26]] [[@sneakers-the-rat:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$1688604806117304yBPhX:matrix.org)):
  - example of using SMW to avoid orphan pages, and also see the query on the tech working group page that makes the table.

https://wiki.social.coop/index.php?title=Update_pass&type=revision&diff=188&oldid=187

You could do something similar with  eg. [[Category:Tech WG]] and [[Category:Tutorial]] but ya if ya set up the views you want in indexing pages then you can make the subpages be self-organizing without a lot of headache of remembering to manually link stuff from multiple places
