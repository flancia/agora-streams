- [[2022-03-10 06:30:58]] [[@borismann:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$f1dgYftVrEQBRY5yDEObIRFmObInW6tURBp-W5b06qY)):
  - [[flancian]]: looks like Aral is using MoaParty and hitting an edge case around having his account suspended https://twitter.com/moaparty/status/1501363324872765442
- [[2022-03-13 21:09:30]] [[@borismann:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$u7c_rtqNpVafpArWdAdAe3JXRkPsD_lPNPvTe6Kzg0I)):
  - [[flancian]]: oh of course! I thought you had already. Admin added 
- [[2022-03-10 23:12:13]] [[@borismann:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$_2Rl1kU04g6r3mfzRN-S3wctEzAp1-ax3ZaomfPdmDk)):
  - [[flancian]]: I was surprised and pleased to see Aral using the service 
- [[2022-03-19 20:28:41]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$d1_RLDye8PFZHpvHbUr_2HKWMAYpNRtq1xzlszvY5rA)):
  - [[flancian]]: moved our discussion about [[analytical seclusion]] to the garden. If you are not ok with that, let me know

=> https://melanocarpa.lesarbr.es/hypha/analytical_seclusion
- [[2022-05-01 21:07:55]] [[@huggingpuppy:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$sPh3hRTfsLUch3xSbzY_jbfTdI7ImtSCKHfaao64z1I)):
  - just had a call with [[flancian]] and [[j0ecool]]

[[agora]] and the [[shamanic agora]] are insanely auspicious

a cheers to the open ethos 🥂

as it was, as it is, as it shall be ✨
- [[2022-05-01 21:16:27]] [[@huggingpuppy:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$J4D-uNZTer_xYF2oul7-qe741cv2nGyJQVwcEK5WSws)):
  - just had a call with me ([[kasra]]) [[flancian]] and [[j0ecool]]

[[agora]] and the [[shamanic agora]] are insanely auspicious

a cheers to the open ethos 🥂

as it was, as it is, as it shall be ✨
