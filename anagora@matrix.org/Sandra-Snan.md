- [[2022-04-17 00:07:00]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$95ZTS6dSsGUONx_jSrsl5pO7A8uAuIX9vXknOf-bLh4)):
  - https://social.coop/@flancian/108143302117453479

Interesting discussion here with [[Sandra Snan]]
- [[2022-04-17 00:10:36]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$4Wo6wSON7fVmjR3slKwQZyAXhtXUQL5goWKEKzsvDMY)):
  - [[Sandra Snan]] has a digital garden [1] and a [[gemlog]] that outlived mine.

[1] https://idiomdrottning.org/
- [[2022-05-02 02:16:29]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$OMBLrelSv3exGqAlAmVw4ikbr-5kToesoQdpw3U-J0M)):
  - [[Sandra]] = [[Sandra Snan]]
