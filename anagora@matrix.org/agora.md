- [[2022-03-13 20:20:18]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$DtxAtvSFUy87P8Zvo808wq6j6sWcroLc8VoTuial2jE)):
  - another project of mine, [[agora]], has a matrix bot implemented on maubot (python)
- [[2022-03-14 00:18:23]] [[@borismann:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$JIObB13_F8fV9XoOdD8Oh5XrAmls_lpkkUl3_LdhBRY)):
  - I found myself in the [[agora]]
- [[2022-03-26 17:27:58]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$W2jQkLbEy6KIcY5EgQONqfFTRSUvo_7iF1hFtkWJQPw)):
  - I stopped working on it altogether when I started coding the [[agora]], 1.5y ago.
- [[2022-04-10 14:00:39]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$FGMKDSGODyUBAEqss-f-yhrgHpHMk6xoZ8LHg7NTac4)):
  - the [[agora]] root repository does not as it's configuration + writing only. the same for [[flancia org]].
- [[2022-04-10 17:10:50]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$ci6sGHWM4uom1OFTitq0-RwKVWpQ25laeR-nWaVt8UE)):
  - this is what I think summarizes my position w.r.t. the [[entaglemente]] between [[google]] and the [[agora]]:
- [[2022-04-16 22:22:37]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$Qir7I8yBkPdcMBebloj8qqZvvTnBqL1mdQxOcDZezcc)):
  - if you are in [[discord]], you can bridge a channel to a matrix room; or we could develop a Discord version of the Agora bot if you're interested. you could also run your own [[agora]] if you want; it's all open source.
- [[2022-05-01 21:07:55]] [[@huggingpuppy:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$sPh3hRTfsLUch3xSbzY_jbfTdI7ImtSCKHfaao64z1I)):
  - just had a call with [[flancian]] and [[j0ecool]]

[[agora]] and the [[shamanic agora]] are insanely auspicious

a cheers to the open ethos 🥂

as it was, as it is, as it shall be ✨
- [[2022-05-01 21:16:27]] [[@huggingpuppy:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$J4D-uNZTer_xYF2oul7-qe741cv2nGyJQVwcEK5WSws)):
  - just had a call with me ([[kasra]]) [[flancian]] and [[j0ecool]]

[[agora]] and the [[shamanic agora]] are insanely auspicious

a cheers to the open ethos 🥂

as it was, as it is, as it shall be ✨
- [[2022-05-06 02:32:03]] [[@huggingpuppy:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$6kV10QQHNGqOpOjmXmFD_nGHr5PL2mpfp2V-Ym4ZH4o)):
  - this might have a really cool [[real-time]] [[integration]] with [[agora]] https://twitter.com/_jzhao/status/1507377585038446602?s=21&t=WHdJUx-nkT__Mo5H0yuSHA
- [[2022-05-06 23:26:17]] [[@huggingpuppy:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$t78EOzryXF91qOcct7ARnRKzawqjXH5VhMdWzaAQkG4)):
  - feels like it would be more useful to have [[cursor chat]] on the [[agora]] page directly?
- [[2022-05-07 00:27:45]] [[@vera:fairydust.space]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$OiRKbn5ukqJJmONAZbOtZ1QU5qRrw08-cjWjoyuCFic)):
  - [[twitter]] [[mastodon]] [[agora]]
- [[2022-05-11 19:18:59]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$0JOzvHE_yconSMLusTPAfJhw9ru-ZVizaRvGRm_dn6I)):
  - also the [[agora]] and [[moa]] are both python, and we are targetting the same architecture
- [[2022-05-11 19:19:19]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$6PHPKhbN2JxHwwocRbpcpUKUu3N8Dm8oFgnYWNUss94)):
  - the [[agora]] is a repository of repositories :)
- [[2022-05-22 00:51:14]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$lCN36gyL5tygUj0GnKuj-RYP9xNuW0WcAYov1JJp9kk)):
  - you should find that the bot does not respond to hashtags until after you tweet something with either #optin or #agora  
- [[2022-05-22 05:27:35]] [[@metasj:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$5omwqC0uWGungO_CStO_paI9NH5cyrpawNgkc52zFIQ)):
  - Correction: after writing #agora it reviewed all of my posts in that theead, even those an hour before, and responses to them all at once
- [[2022-05-27 00:19:18]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$jspMUYtlAcIqaOsgG5Fa0JEFPgnGP3JRiYLWPgSViEc)):
  - et voila, a containerized [[agora]] :) http://hypatia.anagora.org:8000/
- [[2022-07-09 18:05:36]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$VSkYY-TeHs1VBcn64HeflXi4jD_fgI2ovbIXjsxxIwc)):
  - you mean that it's confusing that [[go/agora]] is the root repo?
- [[2022-07-09 18:06:03]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$vXcjXM-Zo2kDXscAmYPi3K7MQUrVUTXqj7OylTSQcZw)):
  - maybe [[go/agora]] should point to a git repo that has agoras in sources.yaml
- [[2022-07-09 18:06:44]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$NEQq8_H_kqGFYZkR5AZ8r3ImgWNSC8sMxDLTFqJGdDU)):
  - I believe [[go/agora]] should be the software
- [[2022-07-10 13:50:10]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$cNfeL7LxQIXh_GV55hvzbW07b0vbMhaYcEhtGdR5-Q8)):
  - would you object to calling the [[agora]] a [[promiscuous protocol]]? yes/no/maybe
- [[2022-08-16 19:12:50]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$N0yVSsSTo-kk7R0uCB_rgN6bbqjV5xkA9DsUl_rUWP8)):
  - [[gordon brander]] is building an [[agora]]!!!
- [[2022-11-14 00:38:54]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166838273477763RFCOp:matrix.org)):
  - I am partial to my own wiki-like system, the [[agora]], which also has the advantage of being updatable from [[matrix]] and [[mastodon]] ;)
- [[2022-11-18 22:03:20]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!zPwMsygFdoMjtdrDfo:matrix.org/$WCqFY_tZF3sEBviBh5fohZr5iwmK00CJA3OsVN3Xxsk)):
  - at the same time I'll be trying to finish containerizing the [[agora]], my personal project. if you are interested in following along, I plan to [[work in public]].
- [[2022-11-28 21:32:55]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$1669667575155379Ztltf:matrix.org)):
  - dphiffer: I try to keep all my ethically charged positions about my employer in the [[agora]] :)
- [[2023-02-17 21:42:21]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$EOTSjrPHb9484AY2TZpT5X6T6zWbm-1SSuxCbxJTk9M)):
  - full disclosure: until you convince me otherwise, I'm totally storing a sqlite file right there in [[go/agora]] ;)
- [[2023-03-07 22:53:08]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$xbDc8FWPrcVBQC99tqyN_5R00Kcz2L9A6Mlgqv5ad9M)):
  - I am working on a presentation for Thursday but after that I hope to get my feet wet with [[coop cloud]] / [[abra]] and with that unlock setting up a variety of services for [[social coop]] and the [[agora]] community.
