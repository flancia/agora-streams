- [[2023-02-17 21:50:30]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$jYncaNE2MlfLoohPLXCc9ytKd4St6S1CAREHWdecTAQ)):
  - Just to be sure, you want [[Agora 2023]] to?:

1. Change the storage medium to something based on [[SQLite]]

2. Support more formats, including [[Mycomarkup]]

3. Fetch through more media, not just [[Git]], but also parsing [[microformats]] and whatnot
- [[2023-02-17 21:50:48]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$2Ftn1c-c-8QdLXK55awYI4y9ssGRhVq_bO3BgFb7LR4)):
  - Just to be sure, you want [[Agora 2023]] to?:

1. Change the storage medium to something based on [[SQLite]]

2. Support more formats, including [[Mycomarkup]]

3. Fetch subnodes through more media, not just [[Git]], but also parsing [[microformats]] and whatnot
