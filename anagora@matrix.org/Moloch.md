- [[2022-08-10 19:28:04]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$J2G0cbEB4TA_owVca6vvpg2ttlwBy3WgYk2j69wbIpc)):
  - https://melanocarpa.lesarbr.es/hypha/leviathan [[leviathan]]

> Leviathan (לויתן) is one of the rulers of Babylon. It is a sea monster, a large serpent with impenetrable scales. Leviathan does not give you choice, it just does what it intends to. It can destroy your home, kill somebody, but it is not to blame, for it is more powerful, than you.

Do we have Leviathan in our mythology? A friend of [[Moloch]]'s, so it seems.
- [[2023-01-20 20:24:53]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$167424269315905TAMvS:matrix.org)):
  - even if one thinks the VC scene is overall a net negative for society, I say maybe let [[Moloch]] fund the revolution if they want, and let us just worry about making those resources contribute to the greater good.
- [[2024-05-16 21:35:14]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$oQRqFuIZCoZZsmIRC8VjpfVW0oyoTkmz7-hsQV2iqLg)):
  - that's just [[Moloch]] weaving itself into the fabric of society as usual
