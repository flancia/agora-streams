- [[2022-09-26 23:52:13]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$1664229133204574JSfjR:matrix.org)):
  - [[twg]] has links to the tech group wiki (there's several in social.coop, it was non-trivial to find it), meeting minutes, etc.
- [[2022-10-08 16:04:04]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$NwJaVIhRxvKOVrVDui-l46ul02upGC4W18NOInY8ImY)):
  - [[social coop]] work: [[twg]] with an associated short document I want to share
- [[2022-10-08 17:58:03]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166524468375418kreaI:matrix.org)):
  - ahoy there! finally getting to allocate some time to [[twg]] this weekend, happy about it :)
- [[2022-10-17 20:49:35]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166603257597589tjyMt:matrix.org)):
  - we took some AIs to advance the [[mastodon upgrade]] plan forward, including also renaming the document as it's growing into more of an execution plan/roadmap for the [[twg]].
- [[2022-11-04 00:39:11]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166751875156062JxAPE:matrix.org)):
  - I think it could be cool to set up an oncall structure for the [[twg]] the same way the [[cwg]] has it by the way
- [[2022-11-06 20:23:37]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$166776261760633dNsdQ:matrix.org)):
  - we were discussing this in the [[twg]]; apparently only ~1/3rd of members have donated through open collective.
- [[2022-11-08 13:29:36]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$1667910576112823NbtWk:matrix.org)):
  - my recommendation would be to continue the discussion in the [[twg]] chat room
- [[2022-11-14 00:41:57]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166838291777826wMhid:matrix.org)):
  - yes, unless the current system is actively getting in the way I'd just keep it as primary for now? meaning https://git.coop/social.coop/tech/operations/-/tree/master as the 'root' for the [[twg]] documentation, the place where you should be able to start reading (and keep reading 'leaves') and traverse all our docs.
- [[2022-11-14 00:46:14]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166838317477909mJQGO:matrix.org)):
  - seeding one in the stoa for [[twg]] if you are OK with that:
- [[2022-11-14 16:41:48]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166844050898350piHpd:matrix.org)):
  - which renders in [[social coop tech group]] and [[twg]] in an Agora.
- [[2022-11-18 21:00:21]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$166880162150038qPHUa:matrix.org)):
  - thank you so much for taking care of this, protean! the [[twg]] and the whole community benefit greatly from your presence!
- [[2022-11-18 23:09:19]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$166880935952920PyvWz:matrix.org)):
  - if you (the [[twg]]) agree I'll start a Loomio proposal to have working groups have a discretionary monthly budget until the community votes to cancel (e.g. if we have financial troubles). ntnsndr does that make sense?
- [[2022-11-24 00:46:18]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$J-EQX-uiGwKp8bkc2QgpOoT_2UXYOIqVoq02uEr9g2w)):
  - I've been working for social.coop after work these last few days -- interesting stuff happening over there :) I'm a member of [[twg]] and [[cwg]]. oncall for the latter this week.
- [[2022-11-25 20:17:50]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$166940387087531jgeDQ:matrix.org)):
  - Nice example of how the [[twg]] would like to run proposals for spending our discretionary [[budget]]
- [[2023-01-07 21:57:44]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!akkaZImONyQWKswVdt:matrix.org/$167312506435556eAVSj:matrix.org)):
  - I've seen it before but not for a while -- I'll bring it up in the [[twg]] room, these should probably show up as 429s in monitoring? davidvasandani edsu do you know?
- [[2023-03-15 21:37:55]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!aIpzDTRzEEUkMCcBay:matrix.org/$167891267577001oUeQA:matrix.org)):
  - we seem to be having an issue with our SSL certificate. investigating in the [[twg]] chat
