- [[2022-11-17 04:49:39]] [[@charismatic_shell:matrix.org]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$RzcEcviMRfbYpJE7kmWV1XrJvEHnVXfpgPSUYPCdVkI)):
  - I have a new project idea, a tool to manage your link collection, called [[Betula]], that can federate with other Betulae and Mycorrhizae, thus emerging the [[Mycoverse]].

I try not to jump into implementing it. I have a lot of assignments, including programming ones. If I work on Betula instead, I'll feel bad.

Accepted two patches to [[Mycorrhiza]] with [[SourceHut]] recently. Pretty fun.
- [[2022-11-17 10:21:50]] [[@handlerug:handlerug.me]] ([link](https://develop.element.io/#/room/!WhilafaLxfJNoigHCj:matrix.org/$hTLJ2H7GSRqh-ExpMQt-TQL2Bfj1yeSgU8S_9atdLRg)):
  - Sent two patches to [[Mycorrhiza]] with [[SourceHut]] recently. Pretty fun.
