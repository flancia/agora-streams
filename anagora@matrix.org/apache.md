- [[2022-04-13 10:58:43]] [[@flancian:matrix.org]] ([link](https://develop.element.io/#/room/!LrSlpgPruzcRvucsiG:matrix.org/$WyEJdVzW12avXK_ptYm_xiQ07aWqu5RqY2NudHLCFwA)):
  - for 1, the license is [[apache]] -- which honestly is good enough for me. I have historically been a fan of GNU, but MIT/BSD style seems fair enough and probably more flexible for some.
