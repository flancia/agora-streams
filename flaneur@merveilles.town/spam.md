- [[2024-05-08 20:32:08+00:00]] @[[flaneur@merveilles.town]] (<a href='https://merveilles.town/@flaneur/112407368704931248'>link</a>):
  - <p>I'm moving my account back to public by default -- I was posting as unlisted earlier. </p><p>Please let me know if you think I should [[spam]] less or change my patterns of posting, thank you!</p>
