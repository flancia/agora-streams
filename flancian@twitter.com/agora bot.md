- [[flancian]] https://twitter.com/{'id': '773144670507499521', 'name': 'The Agora is a Federated Knowledge Commons', 'username': 'flancian'}/status/1561471669461467139
  - The Twitter [[agora bot]] (@an_agora) will go into maintenance for a while while I merge a feature branch.

Please excuse any delays. It should catch up when back :)- [[flancian]] https://twitter.com/flancian/status/1561471669461467139
  - The Twitter [[agora bot]] (@an_agora) will go into maintenance for a while while I merge a feature branch.

Please excuse any delays. It should catch up when back :)- [[flancian]] https://twitter.com/flancian/status/1575157981985050624
  - Testing new [[agora bot]] Twitter code with direct mentions, please ignore.

@an_agora- [[flancian]] https://twitter.com/flancian/status/1575174929208979457
  - Note that @an_agora is back in limited form, only with explicit mentions support on @twitter: [[flancia meet]].

(This is while we implement OAuth 2.0 User Context support in [[agora bot]] to meet the new [[Twitter API]] requirements and be able to read the bot's own timeline.)
- [[2022-10-01 19:48:49+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1576298105355456512

[[agora bot]] is now fully back to Twitter! That means if you follow @an_agora and then use [[wikilinks]], you don't need to keep at-mentioning the bot; they should see your post in their timeline and react to it within a few minutes. https://t.co/YH4g4KcyCZ

---

- [[2022-11-09 21:40:14+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1590459273867411458

I've let [[agora bot]] fall behind in the Fediverse, in the sense that the Twitter bot is now superior in at least one way: it lets people [[opt in]] to storing not only links to their messages in the mentioned nodes, but also a full copy of the message.

I will try to (1/2)

---

- [[2022-11-17 23:46:18+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593390101446500352

RT @flancian@social.coop
:D it's not much but it's honest work (tm).

This brings the #Fediverse to parity w.r.t. [[agora bot]] functionality, finally!
https://t.co/Vd64DewSjV https://t.co/ObmpSmag7V

---

- [[2022-11-17 23:41:25+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593388873572552704

:D it's not much but it's honest work (tm).

This brings the #Fediverse to parity w.r.t. [[agora bot]] functionality, finally! https://t.co/6sQ4xqf6ci

---

