- [[2022-11-05 15:09:45+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1588911454346153986

Shipped some [[Agora UI]] changes:

1. Re-added old-school subnode headers; they make it clearer that each subnode is a separate file contributed by a user.
2. On empty nodes, promote the Stoa section and automatically pull the Hedgedoc (where users can write without logging in). https://t.co/ifyrfSfviX

---

