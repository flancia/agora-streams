- [[2022-10-14 22:52:15+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1581055313096364034

I had a great time at [[fellowship of the link]] today. Among other [[tools for thought]] we discussed [[cosma]].

Thank you @jerrymichalski for getting the group back together! https://t.co/gdqeRyx1PM

---

- [[2022-10-26 19:26:21+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585352148455391268

Today was the [[fellowship of the link]] weekly, it was great!

Minutes can be found in the Agora of Flancia as usual. [[fotl threads]] tracks longer term threads of discussion, being seeded currently.

Thank you [[[peter kaminski]] [[chris aldrich]] [[bentley davis]] (1/2)

---

- [[2022-11-16 20:40:03+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1592980841684033536

I write this on the way back home from work and a beautiful[[fellowship of the link]] meeting.

How are you tonight?

---

