- [[2022-11-26 17:59:30+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596564317817786372

On this topic, [[Ward Cunningham]] thinks that it's not correct to let go of HTTP as 'sufficient' for the web as it increases the complexity of implementing a minimally functional client/server pair too much, and it could let a group [[man in the middle]] the internet (as (1/2)

---

