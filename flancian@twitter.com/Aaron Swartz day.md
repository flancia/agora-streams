- [[2022-11-26 23:49:31+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596652399875293184

CW: Death

Next January 11th will be the tenth anniversary of his death then.

Would it make sense to think of calling it an [[Aaron Swartz day]]? We can use it to remember him and think of how to best help people like him in the present and the future.

---

