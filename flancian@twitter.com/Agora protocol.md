- [[2022-11-19 17:59:13+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594027528536338432

My hypothesis is that we need a human readable/semantic protocol layer on top of what pre-internet human writing has evolved to be. Something on the pragmatic layer of linguistics.

I call it [[Agora protocol]]. And I'll try to implement it on top of [[ActivityPub]] these (1/2)

---

- [[2022-11-19 18:13:13+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594031054301675527

RT @flancian@social.coop
My hypothesis is that we need a human readable/semantic protocol layer on top of what pre-internet human writing has evolved to be. Something on the pragmatic layer of linguistics.

I call it [[Agora protocol]]. And I'll try to implement it on top (1/2)

---

