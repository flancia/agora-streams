- [[2022-11-04 20:08:39+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1588624285790527488

If you don't like [[Mastodon]] that's OK, you can fix it.

---

- [[2022-11-17 01:13:02+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593049538369142784

RT @flaneur@merveilles.town
I found this funny :) A friend send it to me, it's from a Spanish satiric site.

"Thousands of people register an interest to study Software Engineering so they can learn to use #Mastodon."
https://t.co/sPHRuMdc0D https://t.co/Iay4hNqQdv

---

- [[2022-11-19 14:42:31+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593978029042069504

https://t.co/nFjOQVEr8Y

Welcome to the Fediverse! :)

#Mastodon
https://t.co/qxn7rTWeor (2/2)

---

- [[2022-11-26 13:10:12+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596491513554685952

The way the #Mastodon web interface changes functionality depending on your screen width is a smell IMHO. It reminds me of [[mystery meat]] navigation.

---

- [[2022-11-26 13:10:07+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596491492243099648

#Mastodon sucks a bit by the way, but:

1. It sucks less than Twitter in many interesting ways.
2. It is developed by humans you can mostly directly talk to.
3. It can be improved by you or your friends, given it's open source.
4. It is only one out of many #Fediverse providers.

---

