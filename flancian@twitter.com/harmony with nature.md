- [[2022-10-28 21:54:03+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1586114095681527817

Principles of [[Liberatory Technology]]:

- Technology should [[reduce toil]]
- Technology should function in [[harmony with nature]]
- Technology should facilitate community and [[reduce alienation]]

h/t @neil@social.coop for https://t.co/W4sXeGgrBs

---

