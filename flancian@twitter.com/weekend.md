- [[2022-10-13 18:02:10+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1580619922421428225

Good evening all!

I'm taking tomorrow off from work to spend time with a friend who is visiting :)

How are you tonight/this upcoming [[weekend]]?

---

- [[2022-10-30 15:58:39+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1586749430698582020

I'll be working in the Agora all the remainder of the [[weekend]], would you like to join me?

---

- [[2022-11-26 15:48:07+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596531251208671232

This is #Flancia in a nutshell: [[four pomodoros]] on a #weekday, [[eight pomodoros]] in a #weekend, for the #révolution

---

