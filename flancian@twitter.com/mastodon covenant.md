- [[2022-11-26 13:34:51+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596497714816040960

#socialcoop *could* be in https://t.co/dzhyz2GwbV if we want to. I think we would need to do two things:

1. Sign the [[mastodon covenant]], being https://t.co/5S96fnAw7P, which looks reasonable.
2. Relax registrations to 'invite only', as discussed earlier in (1/2)

---

