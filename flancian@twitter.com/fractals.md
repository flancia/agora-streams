- [[2022-10-30 16:03:24+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1586750627333513219

Thinking about the nature of [[fractals]] living

---

- [[2022-11-27 18:30:34+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596934521491955712

But maybe I'm confused and what has this property of being intertwined with time is more generally [[computation]], the computation of [[fractals]] being a special case.

---

- [[2022-11-27 18:30:33+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596934517750538240

It feels to me like [[time]] and [[fractals]] are intertwined in nature, are like each other in some meaningful way.

---

