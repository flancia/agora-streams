- [[2022-10-01 12:38:26+00:00]] @[[flancian]] ~ https://twitter.com/flancian/status/1576189798347198464

Happy weekend, all!

Today I plan to work on:

- [[Agora Twitter Bot]] -- try out Tweepy 4 to get away from the relative madness of managing the Twitter API by hand. Maybe add support to "ingest" whole threads if Twitter keep being elusive about allowing access to the timeline.

---

