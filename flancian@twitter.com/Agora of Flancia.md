- [[2022-10-21 14:29:02+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583465386162679808

I'll do this kindly or I'll die trying, this I tell you as an [[Agoran]] of the [[Agora of Flancia]].

---

- [[2022-10-21 14:32:43+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583466313586200582

This I tell you as an Agoran of the [[Agora of Flancia]].

---

- [[2022-11-26 17:35:45+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596558338757885952

[[feature request]] https://t.co/mJmqGM7vap should work and be informative :)

The example Agora has been the [[Agora of Flancia]] so far by default, but to be honest it's quite idiosyncratic and I think that's proven confusing to many

---

- [[2022-12-15 18:38:48+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1603459575830700033

The default [[stoa]] of the [[Agora of Flancia]] (https://t.co/vceujYBOeV) is going down for maintenance (updates), please excuse any disruption :)

---

