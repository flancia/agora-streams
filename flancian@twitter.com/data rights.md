- [[2022-11-20 16:41:37+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594370390154936320

If you liked [[data rights]], you'll love [[compute rights]]: it should be a human right to be able to run arbitrary computational contexts of your own whenever your personal data is handled.

---

