- [[2022-12-01 23:30:05+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1598459449613717505

I believe cross-posting lets us [[build bridges]] to and from walled gardens; it unlocks a form of federation with closed platforms -- for the benefit of their many captive users.

---

