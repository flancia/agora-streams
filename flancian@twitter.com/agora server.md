- [[2022-10-21 15:09:39+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583475611275644929

Over tcp it serves HTTP: a web interface, [[agora server]].

Port [[5018]] is also optimistically claimed to serve an API, [[agora bridge]].

---

- [[2022-10-28 19:03:23+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1586071143701790721

Ha, [[agora server]] has a memory leak you could say :)

But this is just me being sloppy; the leak affects the dev environment I run in the same server (different user). I need to move that elsewhere as it doesn't make sense to cause mini-outages for silly reasons. https://t.co/FjGQLudGoJ

---

- [[2022-11-26 00:27:48+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596299649035685888

The next step is to make /prime/51 execute https://t.co/fe7VSlVeI8 51 in this case. But scripts that don't take any parameters already work here. They execute in the same virtual environment [[agora server]] runs in currently, in an async thread (not the one rendering the (1/2)

---

