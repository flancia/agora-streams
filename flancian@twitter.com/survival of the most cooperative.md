- [[2022-12-29 11:03:57+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1608418538095579139

The Dalai Lama talks about [[survival of the most cooperative]] as the key to human progress, letting us go beyond [[survival of the fittest]], and I think that encapsulates it nicely.

---

