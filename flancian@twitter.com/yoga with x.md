- [[2022-10-01 20:27:29+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1576307836988145666

[[go/yoga with x]] if you want to follow along / practice live with me :)

---

- [[2022-10-09 17:44:29+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1579165920391757824

At least in my playful mind this yields [[yoga with x]] :) https://t.co/2tMnL1Oi3L

---

- [[2022-10-10 21:15:13+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1579581342752591873

Screenshot taken while setting up one of my side projects, [[yoga with x]].

The "fractal interface" makes it so that you can 1. see the past (as the smaller tiles are delayed) or 2. eventually see your friends swapped in if they have also done the episode.

@yogawithadriene https://t.co/p9lNeJllyF

---

- [[2022-10-23 15:29:05+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1584205276269674496

There'll be [[yoga with x]] tonight :)

---

- [[2022-10-26 20:08:22+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585362723017302017

Now I'll do yoga: [[go/move/26]]

[[go/yoga with x]] to join

---

- [[2022-10-27 17:37:14+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585687078901252103

Hot off the presses, a [[yoga with x]] episode (I haven't seen it): https://t.co/urX8Vg2UqF

I think it should have recorded high quality audio, but let's see, I'm new at this :) https://t.co/WVwj7T96Bn

---

- [[2022-11-06 17:20:12+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1589306668915228672

([[yoga with x]] is a side project.)

---

- [[2022-11-18 23:29:03+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593748148790657024

Taking a break from pomodoros to do [[yoga with x]] :)

Live on https://t.co/nOedjtXG6U https://t.co/H5MvdJeDb4

---

- [[2022-12-01 22:41:17+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1598447168888754190

Lady Burup makes an appearance often in [[yoga with x]] ;)

https://t.co/znk8p7EfeO if you ever want to join in! https://t.co/UxOKeDsMOA

---

- [[2022-12-25 19:52:08+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607101909499797504

From the making of [[yoga with x]] tonight :)

https://t.co/TTe4ctGGxB redirects to today's session, thank you yogawithadriene as always :) https://t.co/21C4vVzO4G

---

- [[2022-12-26 19:39:47+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607461190401236994

CW: Flancia

Now I'll do [[yoga with friends]] :)

Today: [[go/yoga with x]] with [[go/move/26]] in the Agora of Flancia.

---

- [[2022-12-26 19:49:13+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607463562242318343

CW: Flancia

Welcome to [[yoga with x]] if you're interested :) https://t.co/8BrGO420Vg

---

