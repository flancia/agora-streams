- [[flancian]] https://twitter.com/flancian/status/1576248816608518144
  - Updating [[Agora bot]], #WorkingInPublic :)

@an_agora https://t.co/LB6jAZWfbn
- [[2022-10-26 19:59:22+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585360456771837953

Except, that is, for the space :)

I started using them everywhere but then moved [[Agora bot]] to write percent encoding in the flavor that uses the symbol + for spaces:

https://t.co/myCrjuYZci

Pro: this is a lossless scheme.
Cons: less readable in many cases, like (1/2)

---

