- [[2022-10-09 17:50:38+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1579167466059890688

[[2022-10-09]] as it happened, a journal entry in the [[Fediverse]]. https://t.co/aRJUHzJBTO

---

- [[2022-11-09 21:33:42+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1590457629096267779

I'm so happy to see so many people coming to the [[Fediverse]]!

It fills my heart with joy.

---

- [[2022-11-16 20:40:04+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1592980845194715136

CW: Flancia

I'll start using [[content earnings]] on the [[Fediverse]].

---

- [[2022-11-17 23:46:18+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593390101446500352

RT @flancian@social.coop
:D it's not much but it's honest work (tm).

This brings the #Fediverse to parity w.r.t. [[agora bot]] functionality, finally!
https://t.co/Vd64DewSjV https://t.co/ObmpSmag7V

---

- [[2022-11-17 23:41:25+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593388873572552704

:D it's not much but it's honest work (tm).

This brings the #Fediverse to parity w.r.t. [[agora bot]] functionality, finally! https://t.co/6sQ4xqf6ci

---

- [[2022-11-19 17:59:12+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594027525482872833

What do you think of the revolutionary potential of the #Fediverse?

For me it's clear: people having control of a [[decentralized]]/[[distributed]] digital [[commons]] yields a liquid democratic platform which can be used to optimize large scale communication for mutual (1/2)

---

- [[2022-11-20 13:55:28+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594328576836472835

.@FirefoxNightly when will you have a #Fediverse presence as well as a Twitter one?

Thank you for developing Firefox! https://t.co/Kp0SJ8wTa4

---

- [[2022-11-20 14:11:52+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594332705641988096

Is there an emerging standard for which subdomain to use for ActivityPub/Fediverse when the domain is already in use for a website/app?

As in, what's the equivalent of the www prefix for the #Fediverse, if there is one?

---

- [[2022-11-20 15:23:05+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594350627399864321

If you want to join the #Fediverse and need a hand, please let me know! :D

And as usual, please reach out over [[matrix]] (@flancian:https://t.co/tE4E0xI6EO) if you need to get a hold of me more quickly.

Thank you!

---

- [[2022-11-25 00:05:47+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1595931717868691458

RT @john@sauropods.win
Big rumblings in the expansion of the #Fediverse have been happening recently. 

#Tumblr has already promised to implement #ActivityPub (the protocol Mastodon uses), which will mean you can follow Tumblr accounts from here and vice versa.  And (1/2)

---

- [[2022-11-26 13:10:07+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596491492243099648

#Mastodon sucks a bit by the way, but:

1. It sucks less than Twitter in many interesting ways.
2. It is developed by humans you can mostly directly talk to.
3. It can be improved by you or your friends, given it's open source.
4. It is only one out of many #Fediverse providers.

---

- [[2022-11-26 13:34:51+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596497713545232387

I may start by putting up a static page which just lists https://t.co/u8q783pHka *alongside with the equivalent sites for every other #Fediverse provider*, and other meta sites (like federation maps).

---

- [[2022-11-26 16:12:23+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596537358760833024

[[Agora Search]] tries to be opt-in search for the #Fediverse.

---

- [[2022-11-27 19:54:33+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596955657093402624

@interstar Good take! I tend to agree.

I think the question at the point in time: will @github federate with other [[git]] + [[cicd]] providers in the #Fediverse or will they stand alone?

---

