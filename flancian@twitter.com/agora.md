- [[flancian]] https://twitter.com/{'id': '773144670507499521', 'name': 'The Agora is a Federated Knowledge Commons', 'username': 'flancian'}/status/1552396170953199618
  - @BillSeitz @bmann @jessmartin @RobertHaisfield @csageland @balOShere @JoelChan86 @TiddlyWiki @an_agora @socialroots_io "Parallel play with forks" as a modality, like that IIUC implemented by [[fedwiki]] and [[agora]] in similar ways, I believe can lower friction significantly towards cooperation and enable bootstrap of a system which supports arbitrary modalities.- [[flancian]] https://twitter.com/{'id': '773144670507499521', 'name': 'The Agora is a Federated Knowledge Commons', 'username': 'flancian'}/status/1552395293785726976
  - @BillSeitz @bmann @jessmartin @RobertHaisfield @csageland @balOShere @JoelChan86 @TiddlyWiki @an_agora @socialroots_io What we need is a [[knowledge commons]]. The commons is based on a shared set of values. A commons may define rules for self-governance. Rules for cooperation can be attached to working spaces, which in an [[agora]] all map to [[wikilinks]] resolved in a [[federated]] system.- [[2022-10-21 14:24:07+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583464151568945153

I promise I'll make the [[agora]] usable for the [[commoners]], meaning among other things: for the non tech oriented, for the non privileged, for [[the people]].

---

- [[2022-11-27 18:33:15+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596935198482268160

an #agora, [[2022-11-27]]

an exploration of the beauty in [[intertwingularity]] https://t.co/2RHnihCF31 https://t.co/6wpre0LGIT

---

- [[2022-12-04 17:19:17+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599453298435784713

RT @flancian@social.coop
smallcircles ocdtrekkie jonny hmiron alcinnz an [[agora]] tries to give communities the tools to solve the so called [[tragedy of the commons]] -- should a tool/device like this be proposed to a standards body?
https://t.co/RIYBCdX8g5

---

