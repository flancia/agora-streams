- [[2022-11-24 22:51:13+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1595912953068486656

There are several sites that do Twitter (followers, following) -&gt; Mastodon account discovery (which is great news in general), but [[debirdify]] seems particularly solid among them (and is open source).

#go https://t.co/4p5bmu4GEK
#git https://t.co/Jx9CbPYa9C

I like the (1/2)

---

- [[2022-11-27 19:54:33+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596955657093402624

@interstar Good take! I tend to agree.

I think the question at the point in time: will @github federate with other [[git]] + [[cicd]] providers in the #Fediverse or will they stand alone?

---

