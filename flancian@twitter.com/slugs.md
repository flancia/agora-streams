- [[2022-10-26 19:52:57+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585358845559377953

Are [[slugs]] a good or bad default for fuzzy entity encoding, in particular in URLs?

As in: https://t.co/sEewLPNZpq

Pro: they keep URLs readable. 
Con: they apply dimensionality reduction (are lossy), but in terms of what they drop are close to what you would naturally (1/2)

---

