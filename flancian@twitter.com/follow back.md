- [[flancian]] https://twitter.com/flancian/status/1575095849155436545
  - Something I will be working on in parallel: soon @an_agora @twitter.com will be back to responding to messages even without mentioning them explicitly if you follow them (they [[follow back]]). But for now, mentions are needed.

If you want a better Agora, consider the Fediverse.