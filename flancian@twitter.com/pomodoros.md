- [[2022-11-06 16:28:12+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1589293582015680512

Interesting split :)

Eight [[pomodoros]] so far today, maybe eight more to go! At least four. https://t.co/TiRPGdy4vL https://t.co/QwXKlweF0p

---

- [[2022-11-06 17:19:46+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1589306561544957952

About to do #yoga for one pomodoro. 

[[pomodoros]] don't have to be about work, they can be a meditation :)

Then I will resume working on [[agora chapter]].

https://t.co/aqwEV5ksjh https://t.co/WjmkWe2mmJ

---

- [[2022-11-19 20:54:25+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594071621588946947

That was roughly four [[pomodoros]] as planned a priori, although I went in many interesting tangents so I stopped counting / felt like I was in [[flow]] for a lot of it :)

---

- [[2022-12-07 20:28:57+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1600588192679346177

I will now do eight #pomodoros, meaning four hours of focus, for the [[revolución Flanciana]].

---

