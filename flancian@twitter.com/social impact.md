- [[2022-11-26 22:30:51+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596632604463210498

Another take: #GitHub has an interesting [[social impact]] programme: https://t.co/eLsHMMw9EV

&gt; The Social Impact team empowers nonprofits and the greater social sector to drive positive and lasting contributions to the world with GitHub products, our brand, and our employees.

---

