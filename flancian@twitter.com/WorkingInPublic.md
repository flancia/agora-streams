- [[flancian]] https://twitter.com/flancian/status/1576248816608518144
  - Updating [[Agora bot]], #WorkingInPublic :)

@an_agora https://t.co/LB6jAZWfbn
- [[2022-12-07 20:22:14+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1600586504908247041

#WorkingInPublic, #BeingInPublic

---

- [[2022-12-25 20:37:56+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607113435879186432

This all as an exercise of #WorkingInPublic, done with [[loving kindness]] :) https://t.co/KLaQhsa2l7

---

- [[2022-12-26 15:39:15+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607400656712646659

#WorkingInPublic on the [[Agora]] tonight.

This is an example of Transclusion in the Agora, as per the pattern by Nelson (1980) and Sutherland (1963). https://t.co/3YyJCABTC3

---

- [[2022-12-26 15:33:08+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607399117516423169

#WorkingInPublic on [[Agora chapter]] tonight. https://t.co/giH2hun5Zs

---

- [[2022-12-27 18:06:17+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607800045969018882

CW: Flancia

#WorkingInPublic in the #Agora

[[2022-12-27]] https://t.co/Dm35ImHktC

---

