- [[2022-11-09 21:40:14+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1590459273867411458

I've let [[agora bot]] fall behind in the Fediverse, in the sense that the Twitter bot is now superior in at least one way: it lets people [[opt in]] to storing not only links to their messages in the mentioned nodes, but also a full copy of the message.

I will try to (1/2)

---

- [[2022-11-17 23:41:23+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593388865628282882

[[opt in]] @agora@botsin.space 

Trying to enable writing to the Agora of Flancia from the Fediverse :)

---

