- [[2022-12-04 18:03:44+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599464486104072193

CW: Flancia

"What do you mean when you say we found [[Maitreya]]?"

From the Chronicles of Flancia, 2023.

---

- [[2022-12-12 18:26:24+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1602369293869617168

Flancia is dedicated to #Avalokiteshvara and to [[Maitreya]].

---

- [[2022-12-14 20:18:07+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1603122181000437762

When it happens, please remember [[Maitreya]].

---

- [[2022-12-14 20:31:22+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1603125514968915970

CW: Flancia

Like [[Maitreya]], [[burning]]!

---

- [[2022-12-16 10:54:21+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1603705083623096320

CW: Flancia

I saw clearly that [[Maitreya]] will disarm [[Moloch]].

---

- [[2022-12-17 20:33:38+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1604213252749111296

Bodhisattva [[Maitreya]]. The figure of Avalokitesvara usually is found in the shrine room near the Buddha image.

https://t.co/yJhww7dtgc (2/2)

---

- [[2022-12-25 17:08:19+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607060683119534080

CW: Flancia

like [[Maitreya]] burning

---

- [[2022-12-25 17:15:24+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607062466025988096

CW: Flancia

Tengo dos jaras: [[Maitreya]] y el [[Agora]].

Las que tengo, soy, o trato.

---

- [[2022-12-26 19:39:46+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607461187309936640

CW: Flancia

I love you [[Maitreya]], thank you my friend!

---

