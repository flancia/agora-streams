- [[2022-11-26 17:10:40+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596552025755766784

Are you aware that everything we write here might be preserved #forever? If humanity and our descendants make it that far.

I like that a lot. But if you don't, maybe consider [[opting out]] publicly so [[future archaeologists]] can make informed decisions.

---

