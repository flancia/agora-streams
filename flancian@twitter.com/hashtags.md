- [[2022-10-14 22:49:39+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1581054657396629504

RT @flancian: In #Flancia there is an #Agora:

https://t.co/zGGLq64tX3
https://t.co/UdykW13v5t

---

- [[2022-10-21 14:24:07+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583464151568945153

I promise I'll make the [[agora]] usable for the [[commoners]], meaning among other things: for the non tech oriented, for the non privileged, for [[the people]].

---

- [[2022-10-21 14:34:45+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583466826616299520

Even when the [[Agora]] is empty, it is full, because it contains an [[internet]].

#2022-10-21 https://t.co/BxATSPGAHT

---

- [[2022-10-21 15:07:05+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583474963109273601

Please let it be known that an Agora defaults to using port [[5017]] tcp and udp.

#push [[agora rfcs]] https://t.co/g2OmB66bKr

---

- [[2022-10-26 19:20:27+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585350665668263949

I took tomorrow and Friday off using time I accumulated oncall for Meet and Workspace and I plan to work on and in the [[Agora]] all this long weekend.

---

- [[2022-10-27 15:53:11+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585660892502441985

I use [[wikilink]] and #hashtags a lot. I love them and I think they are great [[tools for thought]].

---

- [[2022-10-30 22:07:33+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1586842268685651968

root.

They are also a good pattern for when you want to attach metadata to a post while boosting; I often do that by adding [[wikilinks]] or #hashtags. (2/2)

---

- [[2022-11-27 18:33:15+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596935198482268160

an #agora, [[2022-11-27]]

an exploration of the beauty in [[intertwingularity]] https://t.co/2RHnihCF31 https://t.co/6wpre0LGIT

---

- [[2022-12-03 14:51:38+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599053752421457921

I keep looking for a term I knew -- it's somewhere in the [[Agora]].

In its place, I currently have [[mind hazard]] by association with [[bio hazard]].

---

- [[2022-12-04 17:19:17+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599453298435784713

RT @flancian@social.coop
smallcircles ocdtrekkie jonny hmiron alcinnz an [[agora]] tries to give communities the tools to solve the so called [[tragedy of the commons]] -- should a tool/device like this be proposed to a standards body?
https://t.co/RIYBCdX8g5

---

- [[2022-12-04 17:47:02+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599460283147341824

CW: Flancia

I saw an #Agora with [[a million people]] in it

---

- [[2022-12-07 20:34:31+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1600589595393118209

In the [[Agora]] we'll [[flow]] together

---

- [[2022-12-17 18:57:25+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1604189035404361728

Everywhere I go, an [[Agora]] is there with me.

---

- [[2022-12-25 17:15:24+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607062466025988096

CW: Flancia

Tengo dos jaras: [[Maitreya]] y el [[Agora]].

Las que tengo, soy, o trato.

---

- [[2022-12-25 20:37:52+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607113419651530752

An [[Agora]], late [[2022]].

#workinginpublic https://t.co/Ml8kjdYD4L

---

- [[2022-12-26 15:39:15+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607400656712646659

#WorkingInPublic on the [[Agora]] tonight.

This is an example of Transclusion in the Agora, as per the pattern by Nelson (1980) and Sutherland (1963). https://t.co/3YyJCABTC3

---

- [[2022-12-27 18:06:17+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607800045969018882

CW: Flancia

#WorkingInPublic in the #Agora

[[2022-12-27]] https://t.co/Dm35ImHktC

---

