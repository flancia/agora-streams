- [[2022-11-04 19:47:23+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1588618934223527936

One of the beautiful things about [[open source]] is that, if you dream of software, you can make it come true at least in principle. It is a shared space for dreaming, writing, coding together.

---

- [[2022-11-04 20:08:40+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1588624289116618753

But you can also help improve it! It's the beauty of [[open source]].

And sometimes, almost always really, just waiting is enough. As other people are fixing it with you.

---

- [[2022-11-26 22:34:44+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596633579949375488

See also: [[gitlab]], [[gitea]], [[codeberg]] and coops like [[https://t.co/oE2HApAFPH]], all in the same space and building or provisioning [[open source]].

---

- [[2022-11-26 22:30:52+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596632610222100480

See also: [[gitlab]], [[gitea]], [[codeberg]] and coops like [[https://t.co/oE2HApAFPH]], all in the same space and building and provisioning [[open source]].

---

- [[2022-11-27 00:31:31+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596662971354472448

[[karrot]], a free and [[open source]] tool for "#grassroots initiatives and groups of people that want to coordinate face-to-face activities on a local, autonomous and voluntary basis."

https://t.co/B1LdbCn1Vt

---

