- [[2022-10-21 14:34:45+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583466826616299520

Even when the [[Agora]] is empty, it is full, because it contains an [[internet]].

#2022-10-21 https://t.co/BxATSPGAHT

---

- [[2022-11-17 19:45:06+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593329401432776706

I believe it'll be a [[kind revolution]].

The people will peacefully take back control of the global [[commons]] from corporations, starting with the [[internet]] and its governance.

---

