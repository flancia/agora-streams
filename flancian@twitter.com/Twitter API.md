- [[flancian]] https://twitter.com/flancian/status/1575174929208979457
  - Note that @an_agora is back in limited form, only with explicit mentions support on @twitter: [[flancia meet]].

(This is while we implement OAuth 2.0 User Context support in [[agora bot]] to meet the new [[Twitter API]] requirements and be able to read the bot's own timeline.)
