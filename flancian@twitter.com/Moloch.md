- [[2022-10-21 14:33:57+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583466624497389568

This I tell you, [[Moloch]]: I'll slay you if I have to, without killing

---

- [[2022-11-17 19:27:11+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593324894233792512

hasta que [[Moloch]] se nos plante,
se pare de manos,

---

- [[2022-11-19 17:06:06+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594014164158238720

CW: Flancia

I've been telling [[Moloch]] in the face I'm coming, and they don't seem to pay attention to it.

Maybe I'm like a bug about to headbutt a windshield.

---

- [[2022-12-01 23:30:06+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1598459452524560388

"It is not me, but you, who will likely slay [[Moloch]]."

-- from the Chronicles of Flancia, 2023.

---

- [[2022-12-04 18:03:44+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599464483784630272

CW: Flancia

We could stab [[Moloch]] in the heart at first sight if you wanted, but I'd rather we kept a policy of non violence as a default.

Moloch, after all, seems to be an [[egregore]] which evolved with us. As such they're entangled with human culture and bodies. I (1/2)

---

- [[2022-12-14 20:18:05+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1603122172800962560

Of course we will slay [[Moloch]].

---

- [[2022-12-16 10:54:21+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1603705083623096320

CW: Flancia

I saw clearly that [[Maitreya]] will disarm [[Moloch]].

---

- [[2023-01-04 15:22:21+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1610657894608814082

Eran las cuatro y cuarto cuando me acordé de que juntos venceremos a #Moloch.

---

