- [[2022-12-26 15:49:20+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607403197198385152

CW: Flancia

"Maybe we'll [[slay Moloch]] tonight,
but also maybe we'll #save them.

We'll disentangle needless suffering and humanity for good."

From [[The Chronicles of Flancia]], ca. 2027.

---

