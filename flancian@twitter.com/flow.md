- [[2022-11-06 14:13:46+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1589259750818328577

In the Agora we'll [[flow]] for [[good]]!

---

- [[2022-11-19 20:54:25+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594071621588946947

That was roughly four [[pomodoros]] as planned a priori, although I went in many interesting tangents so I stopped counting / felt like I was in [[flow]] for a lot of it :)

---

- [[2022-12-04 14:29:00+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599410444824887296

as we fork we'll [[flow]]

---

- [[2022-12-07 20:34:31+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1600589595393118209

In the [[Agora]] we'll [[flow]] together

---

- [[2022-12-14 20:26:34+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1603124307915583488

CW: Flancia

As we [[merge]] we'll [[flow]]

---

- [[2022-12-25 17:08:18+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607060681177661441

CW: Flancia

as we [[flow]] we'll [[glow]]

---

- [[2022-12-25 17:08:18+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607060679223025664

CW: Flancia

as we [[join]] we'll [[flow]]

---

