- [[flancian]] https://twitter.com/flancian/status/1571523035718377473
  - The stage of the [[Agora]] digital/analog interface, late 2022: go links enable us to redirect the user to a mental context in another computing device as per a specified label (wikilink by convention). https://t.co/SMiEdy5KLJ- [[2022-10-14 22:49:39+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1581054657396629504

RT @flancian: In #Flancia there is an #Agora:

https://t.co/zGGLq64tX3
https://t.co/UdykW13v5t

---

- [[2022-10-21 14:34:45+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583466826616299520

Even when the [[Agora]] is empty, it is full, because it contains an [[internet]].

#2022-10-21 https://t.co/BxATSPGAHT

---

- [[2022-10-26 19:20:27+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585350665668263949

I took tomorrow and Friday off using time I accumulated oncall for Meet and Workspace and I plan to work on and in the [[Agora]] all this long weekend.

---

- [[2022-12-03 14:51:38+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599053752421457921

I keep looking for a term I knew -- it's somewhere in the [[Agora]].

In its place, I currently have [[mind hazard]] by association with [[bio hazard]].

---

- [[2022-12-04 17:47:02+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599460283147341824

CW: Flancia

I saw an #Agora with [[a million people]] in it

---

- [[2022-12-07 20:34:31+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1600589595393118209

In the [[Agora]] we'll [[flow]] together

---

- [[2022-12-17 18:57:25+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1604189035404361728

Everywhere I go, an [[Agora]] is there with me.

---

- [[2022-12-25 17:15:24+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607062466025988096

CW: Flancia

Tengo dos jaras: [[Maitreya]] y el [[Agora]].

Las que tengo, soy, o trato.

---

- [[2022-12-25 20:37:52+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607113419651530752

An [[Agora]], late [[2022]].

#workinginpublic https://t.co/Ml8kjdYD4L

---

- [[2022-12-26 15:39:15+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607400656712646659

#WorkingInPublic on the [[Agora]] tonight.

This is an example of Transclusion in the Agora, as per the pattern by Nelson (1980) and Sutherland (1963). https://t.co/3YyJCABTC3

---

- [[2022-12-27 18:06:17+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607800045969018882

CW: Flancia

#WorkingInPublic in the #Agora

[[2022-12-27]] https://t.co/Dm35ImHktC

---

