- [[2022-10-09 17:42:30+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1579165421626064897

You can think of #Flancia as a well-meaning [[mashup]].

Here, [[go/move/9]] plus [[go/gould]]. https://t.co/fbvTgwGdpI

---

- [[2022-10-14 22:49:39+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1581054657396629504

RT @flancian: In #Flancia there is an #Agora:

https://t.co/zGGLq64tX3
https://t.co/UdykW13v5t

---

- [[2022-10-16 17:07:29+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1581693324733599746

Postcards from [[Flancia]]. https://t.co/Y6ejbr8ZSw

---

- [[2022-10-21 17:05:38+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583504798157971457

#Flancia https://t.co/ZIMhIG99ic

---

- [[2022-10-23 15:27:47+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1584204949843759104

Welcome to #Flancia, population: at least 2.

How are you tonight? https://t.co/WoEtSAHsE1

---

- [[2022-10-27 14:12:46+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585635622219182083

Suddenly you're in #Flancia. This is playing in the background.

https://t.co/HbHQpZkcGl

---

- [[2022-10-27 15:46:25+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585659190894288900

@ChengduLittleA @an_agora I believe #Flancia is what comes after [[Late Stage Capitalism]] upgraded through [[Liquid Democracy]] and [[Fractal Altruism]].

---

- [[2022-10-27 16:45:53+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585674154086465536

#Flancia is what could come after badly regulated, undemocratic [[late stage Capitalism]].

In Flancia people upgraded the world (kindly) using the [[patterns]] Altruism, Federation and Liquid Democracy, initially through the internet, thus provisioning a global [[Commons]].

---

- [[2022-10-27 16:57:17+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585677022927388672

A [[snack]] after doing some heavy lifting.

Now I will try to do [[eight pomodoros]] for #Flancia on a day off. https://t.co/Hc3Q4KxFBS

---

- [[2022-11-05 20:20:30+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1588989657249087488

In #Flancia, [[Elon Musk]] ~ @elonmusk went [[bodhisattva]] in #2023.

---

- [[2022-11-06 22:46:14+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1589388719563603969

[[flan t5]] (believe it or not, bearing no relation to #Flancia... yet) https://t.co/AzkTLbVfaO

---

- [[2022-11-12 16:47:10+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1591472684923240449

I'm out and about in beautiful [[Flancia]] and my phone seems to be close to disintegrating. Photos refuses to work. The camera just froze, so I took a screenshot. https://t.co/LkxhZV9OfE

---

- [[2022-11-17 19:45:05+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593329398672871424

This is one of the plans in the repository of plans that is [[Flancia]].

Would you like to read more about it?

[[yes]] [[no]] [[maybe]]

---

- [[2022-11-20 00:10:58+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594121083007164416

A view of #Flancia, with and without [[night sight]]. https://t.co/RLIJqwkgki

---

- [[2022-11-25 19:47:49+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596229187488174083

The second book of #Flancia has [[51]] chapters.

---

- [[2022-11-26 15:48:07+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596531251208671232

This is #Flancia in a nutshell: [[four pomodoros]] on a #weekday, [[eight pomodoros]] in a #weekend, for the #révolution

---

- [[2022-11-26 15:53:06+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596532505150038016

This is #Flancia in a nutshell too: it's what we try to do in our free time [[for the benefit of all beings]].

---

- [[2022-11-27 12:34:01+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596844793564389381

Live from #Flancia https://t.co/S6XRKwvXng

---

- [[2022-11-28 20:54:53+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1597333227970220032

In some Flancias, #Flancia begins as Mass Effect cosplay. In others it's [[trekkies]] who start it.

---

- [[2022-12-01 19:34:29+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1598400157963661314

#Flancia, o la máquina de hacer [[qualia]] https://t.co/qIuRsyli1m

---

- [[2022-12-03 14:51:37+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599053751238754304

CW: Flancia

Suddenly the device works and you're in #Flancia 

Music is playing in the background.

How are you today?

---

- [[2022-12-27 17:51:42+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607796378297679873

CW: Flancia

With the flag of [[Flancia]].

---

- [[2022-12-27 17:51:42+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607796376376582151

If you don't like the content in this thread, please feel free to skip previous and subsequent posts with the content warning [[Flancia]] :)

---

- [[2022-12-28 15:25:37+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1608122003742932992

The view from the train in #Flancia.)

Going back home early as I'm wiped from yesterday's allergy attack. I'll take half the day off work.

How are you today? https://t.co/2XtxcKZnYB

---

- [[2023-01-06 20:13:30+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1611455943270604803

CW: Flancia

As you get to [[Flancia]] tonight, you see this.

This is playing in the background: https://t.co/ZNmQNm8aDl https://t.co/O4w1NZA9aU

---

