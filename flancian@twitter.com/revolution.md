- [[2022-11-17 19:35:26+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593326968317087752

I sometimes dream that I started the [[revolution]] two years ago at #Google and nobody noticed.

---

- [[2022-11-26 15:53:06+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596532507021053952

I will now try to dedicate the next [[eight years]] of my life to the #revolution, as you help me define it, if you want to.

If we agree on an ethical definition, we can try to work together under many a beautiful banner for the rational and kind re-evolution of the world.

---

- [[2022-12-27 17:51:41+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607796374434619396

I believe I've seen the [[revolution]]. I saw how it happens.

---

