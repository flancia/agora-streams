- [[2022-12-15 19:11:41+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1603467849661616139

CW: Flancia

I think there are thirteen months in the Flancia default [[revolutionary calendar]] and they contain a lot of #Federating and #Commoning.

---

- [[2022-12-15 19:07:14+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1603466733616041985

I think you need thirteen months in a [[revolutionary calendar]]: one month for planning and twelve for executing (in the non-violent sense, close to computing.)

---

- [[2022-12-25 20:37:53+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607113421253656577

Would you like to organize a peaceful [[revolutionary calendar]] together for #2023?

This poll closes on January 1st ;)

---

