- [[flancian]] https://twitter.com/{'id': '773144670507499521', 'name': 'The Agora is a Federated Knowledge Commons', 'username': 'flancian'}/status/1552398182751129600
  - @bmann @jessmartin @BillSeitz @RobertHaisfield @csageland @balOShere @JoelChan86 @TiddlyWiki @an_agora @socialroots_io "[[scaling synthesis]] in a [[knowledge commons]]" :)- [[flancian]] https://twitter.com/{'id': '773144670507499521', 'name': 'The Agora is a Federated Knowledge Commons', 'username': 'flancian'}/status/1552395293785726976
  - @BillSeitz @bmann @jessmartin @RobertHaisfield @csageland @balOShere @JoelChan86 @TiddlyWiki @an_agora @socialroots_io What we need is a [[knowledge commons]]. The commons is based on a shared set of values. A commons may define rules for self-governance. Rules for cooperation can be attached to working spaces, which in an [[agora]] all map to [[wikilinks]] resolved in a [[federated]] system.- [[2023-01-06 20:12:00+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1611455564113911810

RT @flancian: The Agora is an implementation of a [[knowledge commons]]. It lets communities agree on [[sources of truth]], and in ways to…

---

