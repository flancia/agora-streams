- [[2022-10-01 12:38:28+00:00]] @[[flancian]] ~ https://twitter.com/flancian/status/1576189805503074304

- [[Social Coop]] -- the next action towards upgrading the Mastodon instance. That probably means: start a plan document to get it reviewed by people who already went through the process or would also like to learn it in the tech group ([[twg]]).

---

- [[2022-11-17 01:12:56+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593049514696671232

@ntnsndr@social.coop OK to turn up a VPS in digital ocean and incur some additional cost? I'm general, could the [[twg]] get an experiment allowance? Happy to write a proposal but I'd rather go for a dynamic setup up to a threshold (e.g. 50 monthly, although this will be (1/2)

---

