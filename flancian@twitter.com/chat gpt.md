- [[2022-12-04 23:45:54+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599550595320397824

Playing a bit with [[chat gpt]], which is impressive: https://t.co/58c1V78KWC.

By the third interaction I've reached what may become a staple of our future, the moment when you don't know if the AI is volunteering only actual information or also making stuff up (I haven't (1/2)

---

- [[2022-12-05 00:10:56+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599556895492259840

[[chat gpt]] seems really into not having opinions that could be seen as political -- up to and including not caring about human rights. https://t.co/0NJQL0iR2G

---

