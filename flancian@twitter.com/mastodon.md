- [[2022-11-22 12:24:47+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1595030530759983104

Is there any way to configure [[mastodon]] to show number of favorites/boosts for every post in a timeline? 

I know that Mastodon is opinionated (and a bit conservative) w.r.t. how much information is shown by default. But I was assuming there'd be an option to reintroduce (1/2)

---

