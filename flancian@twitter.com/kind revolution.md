- [[2022-11-16 22:28:07+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593008037002084352

I plan to do four pomodoros for the [[kind revolution]] tonight, I'm two in, wish me luck if you may :)

I wish you luck always by default.

How are you tonight?

---

- [[2022-11-17 19:35:26+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593326966664728576

Maybe an [[amazing ratio]] that might be a seed or channel for the [[kind revolution]]?

---

- [[2022-11-17 19:45:06+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593329401432776706

I believe it'll be a [[kind revolution]].

The people will peacefully take back control of the global [[commons]] from corporations, starting with the [[internet]] and its governance.

---

