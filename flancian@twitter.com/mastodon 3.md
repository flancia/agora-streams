- [[2022-11-20 21:46:46+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594447183306457088

Is [[mastodon 4]] generally thought to be more or less performant than [[mastodon 3]]?

We need to do upgrades to the https://t.co/lG1n70TgZr instance in this direction but we're wondering about any potential performance regressions.

---

