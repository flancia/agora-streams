- [[2022-11-17 01:07:14+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593048079086804995

#socialcoop maintenance is coming! We'll start upgrading the Mastodon instance next Monday. I'll publish an announcement soon.

---

- [[2022-11-19 20:54:25+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594071619743498245

Today we made some progress on setting up a new (secondary) server for #socialcoop, and I started learning [[ansible]]: https://t.co/ABRHE0TQMZ

Pretty fun! I'm glad I was finally able to make some time.

---

- [[2022-11-23 11:00:03+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1595371595974946817

#socialcoop wdyt about the update to Mastodon v4 overall? 

My take: some people have noticed unexpected UI changes (I myself don't like the new 'create account' flow too much for example), but overall it seems like an improvement and I'm happy we're now finally up to date (1/2)

---

- [[2022-11-23 12:15:27+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1595390568464523264

RT @eloquence@social.coop
protean Y'all rock! Thanks for the hard work. Now I can finally follow the #socialcoop hashtag :-)
https://t.co/SHeqBDssm4

---

- [[2022-11-23 22:00:43+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1595537858273607683

Trying out [[bonfire]] while I go through #socialcoop registrations (I'm oncall for the community working group this week) :) https://t.co/QfOEVM31nh

---

- [[2022-11-26 13:10:12+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596491510739894272

RT @neil@social.coop
Enjoying the ramping up of #governance discussion that’s been happening in #socialcoop lately.

Reminded of some ideas I really liked from Alison Powell’s book [[Undoing Optimization: Civic Action in Smart Cities]]:

"In contrast to the smooth (1/2)

---

- [[2022-11-26 13:34:51+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596497714816040960

#socialcoop *could* be in https://t.co/dzhyz2GwbV if we want to. I think we would need to do two things:

1. Sign the [[mastodon covenant]], being https://t.co/5S96fnAw7P, which looks reasonable.
2. Relax registrations to 'invite only', as discussed earlier in (1/2)

---

- [[2022-11-26 14:52:18+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596517205075951616

Testing #socialcoop RSS feed :)

If you want to follow this hashtag from an RSS reader, add this feed: https://t.co/SOss1eOT4J

---

- [[2022-11-27 00:05:29+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596656418731368448

(I saw some instance statistics the other day and I'm an outlier post count wise in #socialcoop even though I only joined in August 2020.)

---

- [[2022-11-28 20:54:52+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1597333226254737410

RT @eb@social.coop
#socialcoop logo in place of mastadon's. Thoughts?

cc flancian 

btw: should the https://t.co/lG1n70SJ9T wg's have Guppe groups?
https://t.co/Q44wfWdvxz https://t.co/raFRssSg9o

---

- [[2022-12-08 21:11:43+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1600961342852177935

What's #socialcoop and the wider [[democratic fediverse]] been up to?

---

- [[2022-12-26 00:27:43+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607171261930553344

I'm oncall this week for the #socialcoop [[community working group]] starting on 2022-12-26, please let me know if you need anything!

By default I plan to review registrations and the moderation queue daily.

---

- [[2022-12-27 20:49:47+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607841194586918913

Now reviewing #socialcoop registrations :D

---

