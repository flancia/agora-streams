- [[flancian]] https://twitter.com/{'id': '773144670507499521', 'name': 'The Agora is a Federated Knowledge Commons', 'username': 'flancian'}/status/1552395293785726976
  - @BillSeitz @bmann @jessmartin @RobertHaisfield @csageland @balOShere @JoelChan86 @TiddlyWiki @an_agora @socialroots_io What we need is a [[knowledge commons]]. The commons is based on a shared set of values. A commons may define rules for self-governance. Rules for cooperation can be attached to working spaces, which in an [[agora]] all map to [[wikilinks]] resolved in a [[federated]] system.- [[2022-10-01 19:48:49+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1576298105355456512

[[agora bot]] is now fully back to Twitter! That means if you follow @an_agora and then use [[wikilinks]], you don't need to keep at-mentioning the bot; they should see your post in their timeline and react to it within a few minutes. https://t.co/YH4g4KcyCZ

---

- [[2022-10-30 22:07:33+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1586842268685651968

root.

They are also a good pattern for when you want to attach metadata to a post while boosting; I often do that by adding [[wikilinks]] or #hashtags. (2/2)

---

