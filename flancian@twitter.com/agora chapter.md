- [[2022-11-06 13:26:55+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1589247961313456128

[[working in public]] on the (well meaning, but still quite unreadable) [[agora chapter]] :) https://t.co/LpUdD5VA80

---

- [[2022-11-06 14:08:32+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1589258433471315969

Reading and writing, on [[agora chapter]] :)

I'm chopping it down a lot; it's 29 pages, it could be 20 pages by the end of today again. What I chop I carry to [[agora chapter 2]] ;)

---

- [[2022-11-06 17:19:46+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1589306561544957952

About to do #yoga for one pomodoro. 

[[pomodoros]] don't have to be about work, they can be a meditation :)

Then I will resume working on [[agora chapter]].

https://t.co/aqwEV5ksjh https://t.co/WjmkWe2mmJ

---

