- [[flancian]] https://twitter.com/{'id': '773144670507499521', 'name': 'The Agora is a Federated Knowledge Commons', 'username': 'flancian'}/status/1558834263113580546
  - [[feature request]]: Our Lady of the Burups

@an_agora ~ @agora@botsin.space
when I say Lady Burup I mean [[lady burup]], [[Lady Burup]], [[Marquesa de Burupina]] and all variations- [[2022-10-01 14:37:41+00:00]] @[[flancian]] ~ https://twitter.com/flancian/status/1576219807656996865

Beautiful [[Lady Burup]] https://t.co/y5E4n4OCI5

---

- [[2022-10-15 09:57:41+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1581222772495089664

[[Lady Burup]] surveying new heights https://t.co/1PpqsMRze4

---

- [[2022-10-23 15:29:19+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1584205332766920707

Beautiful [[Lady Burup]], thank you for your company. https://t.co/HPprf2Pozx

---

- [[2022-10-31 10:06:04+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1587023087492628482

Beautiful [[Lady Burup]] after meditating together https://t.co/ZmmiCdztFr

---

- [[2022-11-02 10:59:58+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1587761429188419585

[[Lady Burup]] apparently evaluating if [[Robofriend]] has finished its activities already https://t.co/lodHUKk1Ns

---

- [[2022-11-19 10:32:20+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593915068478996480

Beautiful [[Lady Burup]] https://t.co/63iRYJKKtl

---

- [[2022-11-27 02:37:28+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596694667407822849

[[Lady Burup]] #caturday https://t.co/S7lAAIwaoM

---

- [[2022-11-27 12:21:52+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596841737602965507

I like this dynamic shot of [[Lady Burup]] and yours truly. https://t.co/mU6CQlZiea

---

- [[2022-12-02 10:54:06+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1598631589524787200

[[Lady Burup]] and this Flancian after meditation https://t.co/fCQ1D5vS48

---

- [[2022-12-07 20:06:30+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1600582541869781008

Can you believe this [[Lady Burup]]? I barely can, but then again I'm one of her biggest fans https://t.co/2p0gYTN9s5

---

- [[2022-12-24 11:04:02+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1606606622637309954

#caturday morning with [[Lady Burup]] https://t.co/xrN88HwNFg

---

- [[2022-12-25 18:32:37+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607081900102885379

[[Lady Burup]] (this is portrait mode in the Pixel 5) https://t.co/NvH6KKmPvi

---

- [[2022-12-27 20:12:29+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1607831805503102976

[[Lady Burup]] rests, and motivates me, as I work on Agora chapter. https://t.co/SJWj7fWWk3

---

