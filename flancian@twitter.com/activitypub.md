- [[flancian]] https://twitter.com/{'id': '773144670507499521', 'name': 'The Agora is a Federated Knowledge Commons', 'username': 'flancian'}/status/1552393760964739073
  - @WardCunningham @csageland @BillSeitz @bmann @jessmartin @RobertHaisfield @balOShere @JoelChan86 @TiddlyWiki @an_agora @socialroots_io @Bortseb @kvistgaard about potential of a lingua franca
@mathewlowry is thinking [[activitypub]]

Public [[knowledge feeds]] should probably be available over all federated protocols, IMHO text with links as the bootstrap lingua franca. @an_agora already has some [[matrix]] support.- [[2022-11-20 15:13:04+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594348106245545984

RT @byronalley@mastodon.social
mike Also social.* is definitely my personal vote for where fediverse instances should go by default. It just seems the most natural and neutral, and it's resilient to future protocol changes--ie. if #activitypub is superseded by yet another (1/3)

---

