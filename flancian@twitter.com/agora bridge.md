- [[flancian]] https://twitter.com/flancian/status/1571531434245988353
  - This makes it easier to run and scale, and lets a community-run [[agora bridge]] "takes the heat" when it comes to the complexities of managing personal data (like scaling with the number of connections to platforms, including walled gardens, and optionally circumventing (1/2)- [[flancian]] https://twitter.com/flancian/status/1571524074538516482
  - Then I need to:

- Bring up one new Agora (of [[flancia]], [[shamanic]], [[ogm]]) to test the container setup in https://t.co/BcdFhXFzI2
- Start sqlite experiment in [[agora bridge]], unlocks sign in (maybe I could also use [[moa]] as a base for this though)- [[2022-10-21 15:09:39+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583475611275644929

Over tcp it serves HTTP: a web interface, [[agora server]].

Port [[5018]] is also optimistically claimed to serve an API, [[agora bridge]].

---

