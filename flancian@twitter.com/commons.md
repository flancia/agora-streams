- [[2022-11-17 19:45:06+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593329401432776706

I believe it'll be a [[kind revolution]].

The people will peacefully take back control of the global [[commons]] from corporations, starting with the [[internet]] and its governance.

---

- [[2022-11-19 17:06:08+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594014170759905280

maybe instead of just freedom what we need is freedom in the [[commons]].

---

- [[2022-11-19 17:59:12+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594027525482872833

What do you think of the revolutionary potential of the #Fediverse?

For me it's clear: people having control of a [[decentralized]]/[[distributed]] digital [[commons]] yields a liquid democratic platform which can be used to optimize large scale communication for mutual (1/2)

---

