- [[2022-10-21 14:32:42+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1583466308816973824

Spam spam [[spam]]
If you have to push it, push [[maybe]]

---

- [[2022-10-27 14:17:52+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1585636905474891776

[[yes]] [[no]] [[maybe]] (follow the link to vote)

---

- [[2022-11-16 20:27:38+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1592977717376344065

I sometimes talk and people don't listen. It's like they don't see the point.

Very often it happens when I'm trying to tell them we should cooperate.

Does this happen to you as well?

[[yes]] [[no]] [[maybe]]

---

- [[2022-11-17 19:45:05+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593329398672871424

This is one of the plans in the repository of plans that is [[Flancia]].

Would you like to read more about it?

[[yes]] [[no]] [[maybe]]

---

- [[2022-11-19 16:09:31+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1593999922906681344

The other day I walked in front of the beautiful building of the police of [[kreis 4]] of Zürich and I thought of turning myself in. Would you like to read about it?

[[yes]] [[no]] [[maybe]]

---

- [[2022-12-31 15:39:34+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1609212676248424448

CW: Flancia

Would you trust this guy

#yes #no #maybe https://t.co/a1cuPFn2Py

---

