- [[flancian]] https://twitter.com/{'id': '773144670507499521', 'name': 'The Agora is a Federated Knowledge Commons', 'username': 'flancian'}/status/1552393760964739073
  - @WardCunningham @csageland @BillSeitz @bmann @jessmartin @RobertHaisfield @balOShere @JoelChan86 @TiddlyWiki @an_agora @socialroots_io @Bortseb @kvistgaard about potential of a lingua franca
@mathewlowry is thinking [[activitypub]]

Public [[knowledge feeds]] should probably be available over all federated protocols, IMHO text with links as the bootstrap lingua franca. @an_agora already has some [[matrix]] support.- [[2022-11-19 18:13:19+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594031078716710912

Sorry for being behind on inboxes; I'm looking forward to catching up tonight! I plan to do it tonight after finishing eight pomodoros.

If you need me for anything ever, please DM me on [[matrix]]!

---

- [[2022-11-20 15:23:05+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594350627399864321

If you want to join the #Fediverse and need a hand, please let me know! :D

And as usual, please reach out over [[matrix]] (@flancian:https://t.co/tE4E0xI6EO) if you need to get a hold of me more quickly.

Thank you!

---

