- [[2022-11-26 20:12:15+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596597724698365953

One of the [[Mastodon forks]] that #SocialCoop could test in our second (experimental) instance. https://t.co/0ZJQGEvfAP

---

- [[2022-12-02 20:18:12+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1598773547345772545

The proposal for granting the #SocialCoop [[Tech Working Group]] a discretionary budget, https://t.co/dHZYXp42eQ, passed with no votes against and several interesting discussion threads. Thank you all for participating!

---

- [[2022-12-04 14:21:15+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1599408495618662400

#SocialCoop can we just define the [[Ops Team]] for a given [[Working Group]] as 'whoever shows up to the default weekly meetings'?

---

