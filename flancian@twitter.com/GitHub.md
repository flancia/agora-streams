- [[2022-11-26 22:34:44+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596633581798846464

(This might be an unkind take, as [[GitHub]] is a diverse group of humans. It is meant as a criticism of the corporate entity (now embedded in [[Microsoft]]). No offense intended!)

---

- [[2022-11-26 22:30:51+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596632604463210498

Another take: #GitHub has an interesting [[social impact]] programme: https://t.co/eLsHMMw9EV

&gt; The Social Impact team empowers nonprofits and the greater social sector to drive positive and lasting contributions to the world with GitHub products, our brand, and our employees.

---

- [[2022-11-26 22:30:51+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1596632602617708544

One take: #GitHub is a joke, a company that makes money (and influence) on top of open source but refuses to open source their software.

If I'm not mistaken this is a textbook case of [[enclosure]] of the Commons.

---

