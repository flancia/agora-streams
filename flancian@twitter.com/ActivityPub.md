- [[2022-11-19 17:59:13+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594027528536338432

My hypothesis is that we need a human readable/semantic protocol layer on top of what pre-internet human writing has evolved to be. Something on the pragmatic layer of linguistics.

I call it [[Agora protocol]]. And I'll try to implement it on top of [[ActivityPub]] these (1/2)

---

- [[2022-11-19 18:13:19+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1594031077521170432

of [[ActivityPub]] these following weekends, in some meaningful way.
https://t.co/yG8o6SADPu (2/2)

---

- [[2022-11-25 00:05:47+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1595931717868691458

RT @john@sauropods.win
Big rumblings in the expansion of the #Fediverse have been happening recently. 

#Tumblr has already promised to implement #ActivityPub (the protocol Mastodon uses), which will mean you can follow Tumblr accounts from here and vice versa.  And (1/2)

---

- [[2022-11-28 16:24:09+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1597265096530571264

RT @dakkar@s.thenautilus.net
CW: meta, #federation explanation

Aha! Finally found a clear and complete diagram of activity dissemination in #ActivityPub! It uses Mastodon-specific terms, but the logic works and matches both the code and the behaviour I've seen.
From (1/2)

---

- [[2022-11-28 22:59:19+00:00]] @[[flancian]]: https://twitter.com/flancian/status/1597364545240039424

Very interesting! [[ocappub]]:

https://t.co/KW3PyYLhf7

&gt; In this paper we introduce OcapPub, which is compatible with the original
  [[ActivityPub]] specification.
  With only mild to mildly-moderate adjustments to the existing network,
  we can deliver what we call (1/2)

---

