- [[2022-11-26 17:34:37+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109411308495369935:
  - <p>[[feature request]] <a href="https://example.agor.ai/@user" rel="nofollow noopener noreferrer" target="_blank"><span class="invisible">https://</span><span class="">example.agor.ai/@user</span><span class="invisible"></span></a> should work and be informative :)</p><p>The example Agora has been the [[Agora of Flancia]] so far by default, but to be honest it's quite idiosyncratic and I think that's proven confusing to many</p>
- [[2022-12-15 18:34:55+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109519129528379928:
  - <p>The default [[stoa]] of the [[Agora of Flancia]] (doc.anagora.org) is going down for maintenance (updates), please excuse any disruption :)</p>
- [[2024-01-21 18:32:15+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111795367803362585'>link</a>):
  - <p>[[testing]] the [[Agora of Flancia]], please disregard :)</p>
