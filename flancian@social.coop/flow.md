- [[2022-11-19 20:50:59+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109372444487351464:
  - <p>That was roughly four [[pomodoros]] as planned a priori, although I went in many interesting tangents so I stopped counting / felt like I was in [[flow]] for a lot of it :)</p>
- [[2022-12-04 14:21:46+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109455848651636449:
  - <p>as we fork we'll [[flow]]</p>
- [[2022-12-07 20:21:12+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109474248964798652:
  - <p>In the [[Agora]] we'll [[flow]] together</p>
- [[2022-12-14 20:21:24+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109513885929831838:
  - <p>As we [[merge]] we'll [[flow]]</p>
- [[2022-12-14 20:21:44+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109513887223334136:
  - <p>As we [[merge]] we'll [[flow]]</p>
- [[2022-12-25 16:30:40+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109575264061180375:
  - <p>as we [[join]] we'll [[flow]]</p>
- [[2022-12-25 16:30:52+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109575264799488010:
  - <p>as we [[flow]] we'll [[glow]]</p>
- [[2024-01-30 23:04:40+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111847399812190458'>link</a>):
  - <p>[[yoga with x]] for today, day 29 of [[flow]]: </p><p><a href="https://youtu.be/NY_IkHCwY_c" rel="nofollow noopener noreferrer" translate="no" target="_blank"><span class="invisible">https://</span><span class="">youtu.be/NY_IkHCwY_c</span><span class="invisible"></span></a></p>
