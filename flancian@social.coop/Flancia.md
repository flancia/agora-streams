- [[2022-11-20 00:09:57+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109373226865071234:
  - <p>A view of <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a>, with and without [[night sight]].</p>
- [[2022-11-25 19:45:50+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109406162173697220:
  - <p>The second book of <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> has [[51]] chapters.</p>
- [[2022-11-26 15:30:57+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109410822237499294:
  - <p>This is <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> in a nutshell: [[four pomodoros]] on a <a href="https://social.coop/tags/weekday" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>weekday</span></a>, [[eight pomodoros]] in a <a href="https://social.coop/tags/weekend" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>weekend</span></a>, for the <a href="https://social.coop/tags/r%C3%A9volution" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>révolution</span></a></p>
- [[2022-11-26 15:33:56+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109410833966431923:
  - <p>This is <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> in a nutshell too: it's what we try to do in our free time [[for the benefit of all beings]].</p>
- [[2022-11-27 12:23:23+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109415746984968137:
  - <p>Live from <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2022-11-28 20:54:33+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109423419289742315:
  - <p>In some Flancias, <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> begins as Mass Effect cosplay. In others it's [[trekkies]] who start it.</p>
- [[2022-12-01 19:37:33+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109440103420155954:
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a>, o la máquina de hacer [[qualia]]</p>
- [[2022-12-03 14:42:47+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109450268974013146:
  - <p>Suddenly the device works and you're in <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> </p><p>Music is playing in the background.</p><p>How are you today?</p>
- [[2022-12-03 14:49:15+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109450294448756473:
  - <p><span class="h-card"><a href="https://botsin.space/@agora" class="u-url mention" rel="nofollow noopener noreferrer" target="_blank">@<span>agora</span></a></span> if you are still here: would you be interested if I told you <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> is a [[basilisk]]?</p><p><a href="https://social.coop/tags/yes" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>yes</span></a> <a href="https://social.coop/tags/no" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>no</span></a> <a href="https://social.coop/tags/maybe" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>maybe</span></a></p>
- [[2022-12-15 21:12:16+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109519748210190620:
  - <p>Four pomodoros in working on [[Flancia]] tonight; four more to go. </p><p>Next: [[yoga with x]] -&gt; [[go/move/15]] -&gt; <a href="https://www.youtube.com/watch?v=K1RGa6sW4ME?list=PLui6Eyny-Uzyp5P3Vcuv5qCHQOC8W6grN" rel="nofollow noopener noreferrer" target="_blank"><span class="invisible">https://www.</span><span class="ellipsis">youtube.com/watch?v=K1RGa6sW4M</span><span class="invisible">E?list=PLui6Eyny-Uzyp5P3Vcuv5qCHQOC8W6grN</span></a> "Reset", 20 minutes long.</p>
- [[2022-12-17 19:06:09+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109530576931601609:
  - <p>Tengo dos jaras:</p><p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> y el [[Agora]].</p>
- [[2022-12-25 16:14:18+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109575199711368985:
  - <p>I will now try to do eight hours of focus -- mostly working on [[Agora Chapter]], which I want to finish by tomorrow (which is a public holiday here in <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a>.)</p>
- [[2022-12-27 17:38:16+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109586854498805679:
  - <p>If you don't like the content in this thread, please feel free to skip previous and subsequent posts with the content warning [[Flancia]] :)</p>
- [[2022-12-27 17:39:02+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109586857500149908:
  - <p>With the flag of [[Flancia]].</p>
- [[2022-12-28 15:12:36+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109591944031741198:
  - <p>The view from the train in <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a>.)</p><p>Going back home early as I'm wiped from yesterday's allergy attack. I'll take half the day off work.</p><p>How are you today?</p>
- [[2023-01-04 15:48:50+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109631722631805110:
  - <p><span class="h-card"><a href="https://botsin.space/@agora" class="u-url mention" rel="nofollow noopener noreferrer" target="_blank">@<span>agora</span></a></span> pero en <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> vencimos a [[Moloch]].</p>
- [[2023-01-06 20:09:06+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109644070652421405:
  - <p>As you get to [[Flancia]] tonight, you see this.</p><p>This is playing in the background: <a href="https://anagora.org/go/agora+playlist" rel="nofollow noopener noreferrer" target="_blank"><span class="invisible">https://</span><span class="">anagora.org/go/agora+playlist</span><span class="invisible"></span></a></p>
- [[2023-01-06 20:10:49+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109644077459517421:
  - <p>As you get to [[Flancia]] tonight, you see this.</p><p>This is playing in the background: <a href="https://anagora.org/go/agora+playlist" rel="nofollow noopener noreferrer" target="_blank"><span class="invisible">https://</span><span class="">anagora.org/go/agora+playlist</span><span class="invisible"></span></a></p>
- [[2023-01-15 16:15:53+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109694114441452374:
  - <p>Should I write [[Flancia]] (the book I'm trying to write) in [[English]] (what I use the most currently) o en [[Español]] (mi lengua materna)?</p>
- [[2023-01-16 19:27:03+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109700528407483554:
  - <p>When I visit <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> I sometimes [[light two candles which look like three from some angles]].</p>
- [[2023-01-16 19:41:22+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109700584728050518:
  - <p>When I visit <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> I sometimes light [[two candles which look like three]] from some angles.</p>
- [[2023-01-17 21:02:33+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109706566258795049:
  - <p>I will now do [[four pomodoros]] for the revolution in <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a>.</p>
- [[2023-02-08 21:25:12+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109831226185312083:
  - <p>As you get to <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> tonight, [[Bonnie Tyler]] is playing in the background.</p><p><a href="https://music.youtube.com/watch?v=fSeFxzeo5Ts&amp;feature=share" rel="nofollow noopener noreferrer" target="_blank"><span class="invisible">https://</span><span class="ellipsis">music.youtube.com/watch?v=fSeF</span><span class="invisible">xzeo5Ts&amp;feature=share</span></a></p>
- [[2023-02-16 18:52:11+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109875922957545239:
  - <p>To <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> and beyond!</p>
- [[2023-02-16 19:05:49+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109875976599414239:
  - <p>Would you like to see the heart of <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> tonight?</p>
- [[2023-02-16 19:07:29+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109875983090208597:
  - <p><a href="https://social.coop/tags/yes" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>yes</span></a>:</p><p>In <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> there is a [[Shambala Warrior]] who identifies as <a href="https://social.coop/tags/Avalokiteshvara" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Avalokiteshvara</span></a></p>
- [[2023-02-16 21:53:55+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109876637581875388:
  - <p>In <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> my standard of living is a human right, sustainably.</p>
- [[2023-02-21 21:58:28+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/109904967009521418'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> is [[Creative Commons]] and an exercise of [[Working in Public]].</p><p>Here, an application of what I call <a href="https://social.coop/tags/Protopoi" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Protopoi</span></a>, being an operation of incremental improvement, in this case some cleanup and updates for the rolling calendar.</p>
- [[2023-02-21 19:09:23+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/109904302136223890'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> tonight</p>
- [[2023-12-15 22:57:07+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111586903811229510'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> </p><p>reading about [[ostatus]], looking into the history of the [[Fediverse]] a bit</p>
- [[2023-12-25 16:47:12+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111642072318179665'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> is the reform against <a href="https://social.coop/tags/Moloch" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Moloch</span></a></p>
- [[2023-12-29 15:18:26+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111664372556764994'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2024-01-19 17:37:23+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111783827407845705'>link</a>):
  - <p>Winter in <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2024-01-21 15:49:28+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111794727694272463'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2024-01-30 15:36:29+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111845637463098628'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2024-02-01 20:01:14+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111858003105593874'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> is a well intentioned [[coup d'etat]] against all unethical, oppressive states</p>
- [[2024-02-19 19:44:45+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111959859899436541'>link</a>):
  - <p>If you like [[Flancia]], please boost your favourite posts / [[nodes]] as you wish :) </p><p>Thank you in any case!</p>
- [[2024-03-01 17:36:57+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112021642778943374'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2024-03-01 17:38:13+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112021647764243766'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2024-03-01 22:23:20+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112022768856421714'>link</a>):
  - <p>My <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> is [[distributed federated incremental fractal altruism]], what is yours like? :)</p>
- [[2024-06-14 15:58:28+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112615798097012388'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2024-07-22 18:43:24+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112831614459833480'>link</a>):
  - <p>The last sun of the day in [[Flancia]]</p>
- [[2024-08-10 16:06:08+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112938579942976280'>link</a>):
  - <p>Summer in <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2024-08-16 20:14:39+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112973531034060676'>link</a>):
  - <p>A reminder that <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> is a [[protopia]] :)</p>
- [[2024-08-17 12:58:30+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112977478324188953'>link</a>):
  - <p>Over here in <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> it's a rainy Saturday, which I love. I'm staying home and plan to do some coding in the afternoon.</p><p>How are you today?</p>
- [[2024-10-01 12:03:39+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/113232066576277032'>link</a>):
  - <p>I'll be in <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> until midnight</p>
- [[2024-10-06 17:07:47+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/113261574056801234'>link</a>):
  - <p>The prices in <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> are like these from 1940 but in the future, and all meat is vegan. </p><p>The difference in production cost whenever market conditions make the selling price unsustainable is covered by Flancia Foundation (best-effort/to a limited amount as usual).</p>
- [[2024-10-11 19:50:21+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/113290524851863565'>link</a>):
  - <p>This is Flancia in a nutshell maybe:</p><p>In Flancia my [[Flancia]] is your <a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a></p>
- [[2024-10-25 20:01:40+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/113369841672522892'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> tries to be a [[Distributed Federated Altruistic Fractal Democracy]]</p>
- [[2024-11-01 18:09:28+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/113409036669156368'>link</a>):
  - <p><a href="https://social.coop/tags/Flancia" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>Flancia</span></a> is also an audiovisual project</p>
