- [[2024-09-03 16:44:09+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/113074624858493410'>link</a>):
  - <p>I've been iterating on [[Agora AI]] prompts.</p><p>Note that the Agora AI uses [[Mistral]] for the base model and a prompt that does not include any Agoran data for now; the assistant is getting only the user 'prompt' (query) embellished/put in context.</p>
