- [[2022-11-19 16:23:51+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109371394054137790:
  - <p>Ahora haré ocho [[pomodoros]] para la revolución. </p><p>Cuál es la [[revolución correcta]]?</p>
- [[2022-11-19 20:50:59+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109372444487351464:
  - <p>That was roughly four [[pomodoros]] as planned a priori, although I went in many interesting tangents so I stopped counting / felt like I was in [[flow]] for a lot of it :)</p>
- [[2022-12-04 15:12:11+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109456046887869346:
  - <p>This Flancian will now go back to regular programming ;)</p><p> (I'm doing [[pomodoros]] this Sunday like many, enjoying it. Hope you're having a wonderful day!)</p>
- [[2022-12-07 20:20:11+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109474244949288167:
  - <p>I will now do eight <a href="https://social.coop/tags/pomodoros" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>pomodoros</span></a>, meaning four hours of focus, for the [[revolución Flanciana]].</p>
- [[2023-02-11 14:52:31+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109846668987315700:
  - <p>Being near 4pm I'm about to start doing [[pomodoros]] -- would you like to join me?</p><p>This can mean just an intent to do some [[focused work]] on something that matters to you, if you get the time today!</p>
- [[2023-02-11 14:53:46+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109846673938906600:
  - <p>If you are interested, consider joining the Jitsi room <a href="https://meet.jit.si/pomodoros" rel="nofollow noopener noreferrer" target="_blank"><span class="invisible">https://</span><span class="">meet.jit.si/pomodoros</span><span class="invisible"></span></a></p><p>I'm in it with mic + camera off by default, and I plan to remain in it while I work today.</p><p><a href="https://social.coop/tags/pomodoros" class="mention hashtag" rel="nofollow noopener noreferrer" target="_blank">#<span>pomodoros</span></a></p>
