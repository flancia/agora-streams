- [[2024-08-13 23:02:55+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112957205710470186'>link</a>):
  - <p>Trying out [[openvibe]] on Android!</p><p>Thanks <span class="h-card" translate="no"><a href="https://mastodon.social/@openvibe" class="u-url mention" rel="nofollow noopener noreferrer" target="_blank">@<span>openvibe</span></a></span></p>
- [[2024-08-14 10:03:15+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/112959802251611497'>link</a>):
  - <p>Quote boost in [[openvibe]]</p>
- [[2024-09-24 22:09:53+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/113194814237821025'>link</a>):
  - <p><span class="h-card" translate="no"><a href="https://toot.cafe/@pavo" class="u-url mention" rel="nofollow noopener noreferrer" target="_blank">@<span>pavo</span></a></span> yes, this is a good next step. It could also be done client-side; [[openvibe]] or [[phanpy]] or [[elk]] might be open to such a feature maybe</p>
