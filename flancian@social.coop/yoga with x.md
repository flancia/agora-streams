- [[2022-11-18 22:59:20+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109367286884901581:
  - <p>Taking a break from pomodoros to do [[yoga with x]] :)</p><p>Live on <a href="https://meet.jit.si/yoga-with-x" rel="nofollow noopener noreferrer" target="_blank"><span class="invisible">https://</span><span class="">meet.jit.si/yoga-with-x</span><span class="invisible"></span></a></p>
- [[2022-12-01 22:39:10+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109440817571700146:
  - <p>Lady Burup makes an appearance often in [[yoga with x]] ;)</p><p>anagora.org/go/yoga-with-x if you ever want to join in!</p>
- [[2022-12-23 21:27:26+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109565106375260922:
  - <p>Now about to do [[yoga with x]] with Lady Burup :)</p>
- [[2022-12-25 19:41:28+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109576014292899619:
  - <p>From the making of [[yoga with x]] tonight :)</p><p><a href="https://anagora.org/go/move/25" rel="nofollow noopener noreferrer" target="_blank"><span class="invisible">https://</span><span class="">anagora.org/go/move/25</span><span class="invisible"></span></a> redirects to today's session, thank you @yogawithadriene@twitter.com as always :)</p>
- [[2022-12-26 19:31:57+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109581639200213622:
  - <p>Welcome to [[yoga with x]] if you're interested :)</p>
- [[2022-12-26 19:33:03+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109581643516156313:
  - <p>Welcome to [[yoga with x]] if you're interested :)</p>
- [[2023-01-11 21:10:50+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109672624989104388:
  - <p>Now I'll do [[yoga with x]] with Adriene, Benji and Lady Burup :)</p>
- [[2023-02-03 22:57:04+00:00]] @[[flancian@social.coop]] https://social.coop/@flancian/109803275840623878:
  - <p>[[yoga with x]], [[2023-02-03]] :)</p>
- [[2024-01-30 23:04:40+00:00]] @[[flancian@social.coop]] (<a href='https://social.coop/@flancian/111847399812190458'>link</a>):
  - <p>[[yoga with x]] for today, day 29 of [[flow]]: </p><p><a href="https://youtu.be/NY_IkHCwY_c" rel="nofollow noopener noreferrer" translate="no" target="_blank"><span class="invisible">https://</span><span class="">youtu.be/NY_IkHCwY_c</span><span class="invisible"></span></a></p>
